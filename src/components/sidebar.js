import React from "react"
import {
    Box,
    Image,
    Text,
    List,
    ListItem,
    Button
} from "@chakra-ui/react"
import { Link } from 'react-router-dom';

import AuthAPI from "../models/api/request/auth";

const SidebarItem = ({path, item}) => {
    let isActive = false
    if (item.src === path) isActive = true
    else if (!isActive && path !== "/") {
        if (item.src.includes(path)) isActive = true
        else if (item.src.includes(path.split("/")[1])) isActive = true
    }

    return (!isActive) ?
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="ghost"
            textColor="white"
            flexDirection="row"
            justifyContent="flex-start"
            _hover={{
                bgColor: "#B3A8AC",
                textColor: "black"
            }}>
            {item.label}
        </Button>
    </Link> : 
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="solid"
            bgColor="white"
            flexDirection="row"
            justifyContent="flex-start"
            textColor="green.400">
            {item.label}
        </Button>
    </Link>
}

const SidebarList = ({provider}) => {
    const main_state = provider.main_state
    const menus = main_state.menus.sidebar
    
    const pathname = window.location.pathname

    return (
        <List spacing={2}>
            { menus.map(item => {
                return <ListItem
                key={item.id}>
                    <SidebarItem item={item}
                    path={pathname} />
                </ListItem>
            }) }

            <ListItem>
                <Button size="lg"
                    isFullWidth={true}
                    variant="solid"
                    colorScheme="red"
                    flexDirection="row"
                    justifyContent="flex-start"
                    onClick={() => {
                        let auth = JSON.parse(window.localStorage.getItem(main_state.db))
                        auth.active_key = null
                        AuthAPI.setSession(auth.id, auth, _ => {
                            window.localStorage.removeItem(main_state.db)
                            window.alert("Berhasil keluar")
                            window.location.replace("/")
                        })
                    }}>
                    Keluar
                </Button>
            </ListItem>
        </List>
    )
}

const Sidebar = ({provider}) => {

    const setupDisplay = () => {
        const main_state = provider.main_state
        const base_url = main_state.base_url
        const header = main_state.header

        return (
            <Box paddingY={8}
                paddingX={4}>

                <center>
                    <Image src={base_url + header.logo.main} boxSize={36} />
                </center>
                <br />
                <Text fontSize="lg" dangerouslySetInnerHTML={{
                    __html: header.title
                }}
                fontWeight="bold"
                fontFamily="sans-serif"
                textAlign="center" />

                <br />
                <SidebarList provider={provider} />

            </Box>
        )
    }

    return (
        <React.Fragment>
            { setupDisplay() }
        </React.Fragment>
    )
}

export default Sidebar