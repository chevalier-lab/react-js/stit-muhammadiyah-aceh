import React from "react"
import { 
    Box, 
    Container, 
    CircularProgress,
    Text 
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"

import MainContext from "../utils/provider"
import HomeAPI from "../models/api/request/home"

const Footer = ({footer}) => {
    const [isLoadData, setIsLoadData] = React.useState(false) 
    const [data, setData] = React.useState(null)

    React.useEffect(() => {
        HomeAPI.get(res => {
            if (res.success) {
                setData(res.data[0])
                setIsLoadData(true)
            }
        })
    }, [])

    const setupDisplay = (provider) => {
        const base_url = provider.main_state.base_url

        if (!isLoadData) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat data</Text>
            </Box>
        )

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf data tidak ditemukan</Text>
            </Box>
        )

        return (
            <Box
                bgColor="white"
                marginTop={8}>
                <Container
                maxW="container.xl"
                bgImage={`url('${base_url + footer.motif}')`}
                backgroundRepeat="repeat-x"
                height="44px" />
                <Container
                    maxW="container.xl"
                    paddingY={8}
                    bgColor="#E8E8E7"
                    borderBottom="1px solid rgba(0,0,0,0.1)">
                    <Text textAlign="center">{data.address}</Text>
                    <Text textAlign="center">{data.phone_number}</Text>
                    <Text textAlign="center" fontWeight="bold">{data.copyright}</Text>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default Footer