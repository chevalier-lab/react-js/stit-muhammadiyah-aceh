import React from "react"
import {
    // Button,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    Box,
    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel
} from "@chakra-ui/react"

import CreateCategories from "./create"
import DeleteCategories from "./delete"
import ListCategories from "./list"
import UpdateCategories from "./update"

import CategoriesAPI from "../../models/api/request/categories"

const IndexCategories = ({provider, isOpen, onClose}) => {
    const [currentIndex, setCurrentIndex] = React.useState(0)
    const [currentSelected, setCurrentSelected] = React.useState(null)
    const [categories, setCategories] = React.useState([])

    React.useEffect(() => {
        // Load Data Categories
        loadDataCategories("")
    }, [])

    const onSelectedUpdate = (item) => {
        setCurrentSelected(item)
        setCurrentIndex(2)
    }

    const onSelectedDelete = (item) => {
        console.log(item)
        setCurrentSelected(item)
        setCurrentIndex(3)
    }

    const loadDataCategories = (search="") => {
        CategoriesAPI.filter(0, 10, search, res => {
            if (res.success) {
                setCurrentSelected(null)
                setCurrentIndex(0)
                setCategories(res.data)
            }
        })
    }

    const setupDisplay = () => {

        return (
            <Box>

                <Tabs index={currentIndex}
                    onChange={(index) => setCurrentIndex(index)}>
                    <TabList>
                        <Tab>Daftar Kategori</Tab>
                        <Tab>Tambah</Tab>
                        <Tab>Ubah</Tab>
                        <Tab>Hapus</Tab>
                    </TabList>
                    <TabPanels>
                        <TabPanel>
                            <ListCategories provider={provider}
                                data={categories}
                                onUpdate={onSelectedUpdate}
                                onDelete={onSelectedDelete}
                                onSearch={loadDataCategories} />
                        </TabPanel>
                        <TabPanel>
                            <CreateCategories provider={provider}
                                onSuccess={loadDataCategories} />
                        </TabPanel>
                        <TabPanel>
                            <UpdateCategories provider={provider}
                                selectedData={currentSelected}
                                onSuccess={loadDataCategories} />
                        </TabPanel>
                        <TabPanel>
                            <DeleteCategories onClose={onClose} provider={provider}
                                selectedData={currentSelected}
                                onSuccess={loadDataCategories} />
                        </TabPanel>
                    </TabPanels>
                </Tabs>

            </Box>
        )
    }

    return (
        <Modal isOpen={isOpen} onClose={onClose}
            size="4xl">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Kelola Kategori</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    { setupDisplay() }
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

export default IndexCategories