import React from "react"
import {
    Box,
    Input,
    Text,
    Button
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import CategoriesAPI from "../../models/api/request/categories"

const CreateCategories = ({provider, onSuccess}) => {
    const labelRef = React.useRef()
    const [isCreateCategories, setIsCreateCategories] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onDoCreate = () => {
        setIsCreateCategories(true)
        CategoriesAPI.create(provider.main_state.token, {
            label: labelRef.current.value,
            aria: labelRef.current.value.toString().toLowerCase().replace(" ", "-")
        }, res => {
            if (res.success) {
                labelRef.current.value = ""
                toastSuccess("Berhasil Membuat", res.message)
                onSuccess("")
            } else toastError("Gagal Membuat", res.message)
            setIsCreateCategories(false)
        })
    }

    return (
        <Box>
            
            <Text fontWeight="semibold"
            textAlign="start"
            fontSize="lg">Label:</Text>
            <Input placeholder="Masukkan Label Kategori" size="lg"
            marginBottom={4}
            ref={labelRef}
            type="text" />

            <Button colorScheme="green"
            size="lg"
            isLoading={isCreateCategories}
            onClick={onDoCreate}>
                Simpan
            </Button>

        </Box>
    )
}

export default CreateCategories