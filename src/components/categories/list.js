import React from "react"
import {
    Box,
    InputGroup,
    InputLeftElement,
    Input,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    IconButton
} from "@chakra-ui/react"
import {
    Search2Icon,
    EditIcon,
    DeleteIcon
} from "@chakra-ui/icons"

const ListCategories = ({provider, data, onUpdate, onDelete, onSearch}) => {

    const searchRef = React.useRef();

    const setupFilterTable = () => {

        return (
            <InputGroup
            variant="flushed">
                <InputLeftElement
                pointerEvents="none"
                children={<Search2Icon color="gray.700" />} />
                <Input type="search" placeholder="Cari label kategori..."
                    textColor="gray.700"
                    ref={searchRef}
                    onChange={() => onSearch(searchRef.current.value)} />
            </InputGroup>
        )
    }

    const setupTableHead = () => {

        return (
            <Thead>
                <Tr>
                    <Th>Label</Th>
                    <Th>Aria</Th>
                    <Th>Aksi</Th>
                </Tr>
            </Thead>
        )
    }

    const setupTableBody = () => {
        
        if (data.length === 0)
        return (
            <Tbody>
                <Tr>
                    <Td colSpan={3}>
                        <center>Belum ada data kategori</center>
                    </Td>
                </Tr>
            </Tbody>
        )

        else return (
            <Tbody>
                {data.map((item, position) => {
                    return <Tr
                        key={"item-categories-" + position}>
                        <Td>{item.label}</Td>
                        <Td>{item.aria}</Td>
                        <Td>
                            <IconButton icon={<EditIcon />}
                                colorScheme="yellow"
                                size="sm"
                                marginRight={2}
                                onClick={() => onUpdate(item)} />
                            <IconButton icon={<DeleteIcon />}
                                colorScheme="red"
                                size="sm"
                                marginRight={2}
                                onClick={() => onDelete(item)} />
                        </Td>
                    </Tr>
                })}
            </Tbody>
        )
    }

    const setupTable = () => {

        return (
            <Table border="1px solid rgba(0,0,0,0.1)">
                { setupTableHead() }
                { setupTableBody() }
            </Table>
        )
    }

    return (
        <Box>
            { setupFilterTable() }
            <br />
            { setupTable() }
        </Box>
    )
}

export default ListCategories