import React from "react"
import {
    Box,
    Input,
    Text,
    Button
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"

import { useToast } from "@chakra-ui/toast"

import CategoriesAPI from "../../models/api/request/categories"

const UpdateCategories = ({provider, selectedData, onSuccess}) => {
    const labelRef = React.useRef()
    const [isUpdateCategories, setIsUpdateCategories] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onDoUpdate = () => {
        setIsUpdateCategories(true)
        CategoriesAPI.update(provider.main_state.token,
            selectedData.id, 
        {
            label: labelRef.current.value,
            aria: labelRef.current.value.toString().toLowerCase().replace(" ", "-")
        }, res => {
            if (res.success) {
                labelRef.current.value = ""
                toastSuccess("Berhasil Mengubah", res.message)
                onSuccess("")
            } else toastError("Gagal Mengubah", res.message)
            setIsUpdateCategories(false)
        })
    }

    const setupDisplay = () => {
        if (selectedData === null) 
            return (
                <Box padding={8}
                    alignContent="center"
                    textAlign="center">
                    <WarningIcon boxSize={16}
                        color="yellow" />
                    <Text fontSize="2xl">Perhatian!!</Text>
                    <Text>Harap memilih data di daftar kategori terlebih dahulu</Text>
                </Box>
            )

        else return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Label:</Text>
                <Input placeholder="Masukkan Label Kategori" size="lg"
                marginBottom={4}
                ref={labelRef}
                defaultValue={selectedData.label}
                type="text" />

                <Button colorScheme="green"
                size="lg"
                isLoading={isUpdateCategories}
                onClick={onDoUpdate}>
                    Simpan
                </Button>
            </Box>
        )
    }

    return (
        <Box>
            
            { setupDisplay() }

        </Box>
    )
}

export default UpdateCategories