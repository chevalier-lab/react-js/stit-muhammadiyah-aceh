import React from "react"
import {
    Box,
    Text,
    Button
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"

import { useToast } from "@chakra-ui/toast"

import CategoriesAPI from "../../models/api/request/categories"

const DeleteCategories = ({provider, selectedData, onSuccess}) => {
    const [isDeleteCategories, setIsDeleteCategories] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onDelete = () => {
        setIsDeleteCategories(true)
        CategoriesAPI.delete(selectedData.id, res => {
            if (res.success) {
                toastSuccess("Berhasil Menghapus", res.message)
                onSuccess("")
            } else toastError("Gagal Menghapus", res.message)
            setIsDeleteCategories(false)
        })
    }

    const setupDisplay = () => {
        if (selectedData === null) 
            return (
                <Box>
                    <WarningIcon boxSize={16}
                        color="yellow" />
                    <Text fontSize="2xl">Perhatian!!</Text>
                    <Text>Harap memilih data di daftar kategori terlebih dahulu</Text>
                </Box>
            )

        else return (
            <Box>
                <WarningIcon boxSize={16}
                    color="red" />
                <Text fontSize="2xl">Perhatian!!</Text>
                <Text>Apakah anda yakin ingin menghapus <strong>{selectedData.label}</strong>?</Text>
                <Button colorScheme="red"
                size="lg"
                isLoading={isDeleteCategories}
                onClick={onDelete}
                marginTop={4}>Hapus</Button>
            </Box>
        )
    }

    return (
        <Box padding={8}
            alignContent="center"
            textAlign="center">

            { setupDisplay() }

        </Box>
    )
}

export default DeleteCategories