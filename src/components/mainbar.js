import React from "react"
import {
    Box,
    Container,
    Grid,
    GridItem,
    Button
} from "@chakra-ui/react"
import { Link } from 'react-router-dom';

import MainContext from "../utils/provider"

const MainBarItem = ({path, item}) => {
    let isActive = false
    if (item.src === path) isActive = true
    else if (!isActive && path !== "/") {
        if (item.src.includes(path)) isActive = true
        else if (item.src.includes(path.split("/")[1])) isActive = true
    }

    return (!isActive) ?
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="ghost"
            textColor="white"
            _hover={{
                bgColor: "#B3A8AC",
                textColor: "black"
            }}>
            {item.label}
        </Button>
    </Link> : 
    <Link
        to={item.src}>
        <Button size="lg"
            isFullWidth={true}
            variant="solid"
            bgColor="white"
            textColor="green.400">
            {item.label}
        </Button>
    </Link>
}

const Mainbar = ({menus, path}) => {

    const setupDisplay = (_) => {
        return (
            <Box>
                <Container maxW="container.xl"
                    borderBottom="1px solid rgba(0,0,0,0.1)"
                    bgColor="#425F27"
                    paddingY={4}>
                    <Grid templateColumns="repeat(4, 1fr)" gap={0}>
                        {menus.map(item => {
                            return <GridItem
                                key={item.id}>
                                    <MainBarItem item={item}
                                    path={path} />
                                </GridItem>
                        })}
                    </Grid>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default Mainbar