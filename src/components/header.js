import React from "react"
import { 
    Box, 
    Container, 
    Image, 
    Spacer, 
    HStack,
    Text,
} from "@chakra-ui/react"

import MainContext from "../utils/provider"

const Header = ({data}) => {

    const setupDisplay = (provider) => {
        const base_url = provider.main_state.base_url

        return (
            <Box marginTop={8}>
                <Container
                    maxW="container.xl"
                    paddingY={8}
                    bgColor="#233819"
                    color="white"
                    borderBottom="1px solid rgba(0,0,0,0.1)">
                    <HStack
                        paddingX={6}>
                        <Image src={base_url + data.logo.main} boxSize={36} />
                        <Spacer />
                        <Box>
                            <Text fontSize={{
                                lg: "3xl",
                                md: "2xl",
                                sm: "xl"
                            }} dangerouslySetInnerHTML={{
                                __html: data.title
                            }}
                            fontWeight="bold"
                            fontFamily="sans-serif"
                            textAlign="center" />
                            <Text fontSize="lg"
                            textAlign="center"
                            fontWeight="semibold">"{data.tagline}"</Text>
                        </Box>
                        <Spacer />
                        <Box>
                            <Image src={base_url + data.logo.founder} boxSize={32} />
                            <Text fontSize="lg"
                            textAlign="center"
                            fontWeight="semibold" dangerouslySetInnerHTML={{
                                __html: data.author
                            }} />
                        </Box>
                    </HStack>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default Header