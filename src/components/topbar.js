import React from "react"
import {
    HStack,
    Box,
    Container,
    // Spacer,
    Button
} from "@chakra-ui/react"
import { Link } from 'react-router-dom';

import MainContext from "../utils/provider"

const TopBarItem = ({path, item}) => {
    let isActive = false
    if (item.src === path) isActive = true
    else if (!isActive) {
        item.ariaChildren.forEach(item => {
            if (item.includes(path)) isActive = true
            else if (item.includes(path.split("/")[1])) isActive = true
        })
    }

    return (!isActive) ? 
    <Link
        to={item.src}>
        <Button size="sm"
            variant="ghost"
            _hover={{
                bgColor: "gray.50"
            }}>
            {item.label}
        </Button>
    </Link> : 
    <Link
        to={item.src}>
        <Button size="sm"
            variant="solid"
            colorScheme="green">
            {item.label}
        </Button>
    </Link>
}

const TopBar = ({menus, path}) => {

    const setupDisplay = (_) => {
        
        return (
            <Box 
                borderBottom="1px solid rgba(0,0,0,0.1)"
                bgColor="white">
                <Container 
                    maxW="container.xl"
                    paddingY={2}>
                    <HStack>
                        { menus.map(item => {
                            return <TopBarItem
                            key={item.id}
                            item={item}
                            path={path} />
                        }) }
                    </HStack>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default TopBar