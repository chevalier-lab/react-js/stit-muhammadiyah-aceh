import React from "react"
import {
    // Button,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    Box,
    Tabs,
    TabList,
    Tab,
    TabPanels,
    TabPanel
} from "@chakra-ui/react"

import CreateUsers from "./create"
import UpdateUsers from "./update"
import DeleteUsers from "./delete"
import ListUsers from "./list"

import UsersAPI from "../../models/api/request/users"

const IndexUsers = ({provider, isOpen, onClose}) => {
    const [currentIndex, setCurrentIndex] = React.useState(0)
    const [currentSelected, setCurrentSelected] = React.useState(null)
    const [users, setUsers] = React.useState([])

    React.useEffect(() => {
        // Load Data Categories
        loadDataUsers("")
    }, [])

    const onSelectedUpdate = (item) => {
        setCurrentSelected(item)
        setCurrentIndex(2)
    }

    const onSelectedDelete = (item) => {
        console.log(item)
        setCurrentSelected(item)
        setCurrentIndex(3)
    }

    const loadDataUsers = (search="") => {
        UsersAPI.filter(0, 10, search, res => {
            if (res.success) {
                setCurrentSelected(null)
                setCurrentIndex(0)
                setUsers(res.data)
            }
        })
    }

    const setupDisplay = () => {

        return (
            <Box>

                <Tabs index={currentIndex}
                    onChange={(index) => setCurrentIndex(index)}>
                    <TabList>
                        <Tab>Daftar Pengguna</Tab>
                        <Tab>Tambah</Tab>
                        <Tab>Ubah</Tab>
                        <Tab>Hapus</Tab>
                    </TabList>
                    <TabPanels>
                        <TabPanel>
                            <ListUsers provider={provider}
                                data={users}
                                onUpdate={onSelectedUpdate}
                                onDelete={onSelectedDelete}
                                onSearch={loadDataUsers} />
                        </TabPanel>
                        <TabPanel>
                            <CreateUsers provider={provider}
                                onSuccess={loadDataUsers} />
                        </TabPanel>
                        <TabPanel>
                            <UpdateUsers provider={provider}
                                selectedData={currentSelected}
                                onSuccess={loadDataUsers} />
                        </TabPanel>
                        <TabPanel>
                            <DeleteUsers onClose={onClose} provider={provider}
                                selectedData={currentSelected}
                                onSuccess={loadDataUsers} />
                        </TabPanel>
                    </TabPanels>
                </Tabs>

            </Box>
        )
    }

    return (
        <Modal isOpen={isOpen} onClose={onClose}
            size="6xl">
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Kelola Pengguna</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    { setupDisplay() }
                </ModalBody>
            </ModalContent>
        </Modal>
    )
}

export default IndexUsers