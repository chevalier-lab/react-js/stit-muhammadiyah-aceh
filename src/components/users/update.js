import React from "react"
import {
    Box,
    Input,
    Text,
    Button,
    Select
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"

import { useToast } from "@chakra-ui/toast"

import UsersAPI from "../../models/api/request/users"

const UpdateUsers = ({provider, selectedData, onSuccess}) => {
    const usernameRef = React.useRef()
    const passwordRef = React.useRef()
    const typeRef = React.useRef()
    const [isUpdateUsers, setIsUpdateUsers] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onDoUpdate = () => {
        setIsUpdateUsers(true)
        UsersAPI.update(provider.main_state.token,
            selectedData.id, 
        {
            username: usernameRef.current.value,
            password: passwordRef.current.value,
            type: typeRef.current.value,
        }, res => {
            if (res.success) {
                usernameRef.current.value = ""
                passwordRef.current.value = ""
                typeRef.current.value = ""
                toastSuccess("Berhasil Mengubah", res.message)
                onSuccess("")
            } else toastError("Gagal Mengubah", res.message)
            setIsUpdateUsers(false)
        })
    }

    const setupDisplay = () => {
        if (selectedData === null) 
            return (
                <Box padding={8}
                    alignContent="center"
                    textAlign="center">
                    <WarningIcon boxSize={16}
                        color="yellow" />
                    <Text fontSize="2xl">Perhatian!!</Text>
                    <Text>Harap memilih data di daftar pengguna terlebih dahulu</Text>
                </Box>
            )

        else return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Username:</Text>
                <Input placeholder="Masukkan Username" size="lg"
                marginBottom={4}
                ref={usernameRef}
                defaultValue={selectedData.username}
                type="text" />

                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Password:</Text>
                <Input placeholder="Masukkan Password" size="lg"
                marginBottom={4}
                ref={passwordRef}
                defaultValue={selectedData.password}
                type="text" />

                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Jenis Pengguna:</Text>
                <Select placeholder="Pilih jenis pengguna" size="lg"
                marginBottom={4}
                defaultValue={selectedData.type}
                ref={typeRef}>
                    <option value="admin">Administrator</option>
                    <option value="writer">Writer</option>
                </Select>

                <Button colorScheme="green"
                size="lg"
                isLoading={isUpdateUsers}
                onClick={onDoUpdate}>
                    Simpan
                </Button>
            </Box>
        )
    }

    return (
        <Box>
            
            { setupDisplay() }

        </Box>
    )
}

export default UpdateUsers