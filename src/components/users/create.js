import React from "react"
import {
    Box,
    Input,
    Text,
    Button,
    Select
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"

import UsersAPI from "../../models/api/request/users"

const CreateUsers = ({provider, onSuccess}) => {
    const usernameRef = React.useRef()
    const passwordRef = React.useRef()
    const typeRef = React.useRef()
    const [isCreateUsers, setIsCreateUsers] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onDoCreate = () => {
        setIsCreateUsers(true)
        UsersAPI.create(provider.main_state.token, {
            username: usernameRef.current.value,
            password: passwordRef.current.value,
            type: typeRef.current.value,
        }, res => {
            if (res.success) {
                usernameRef.current.value = ""
                passwordRef.current.value = ""
                toastSuccess("Berhasil Membuat", res.message)
                onSuccess("")
            } else toastError("Gagal Membuat", res.message)
            setIsCreateUsers(false)
        })
    }

    return (
        <Box>
            
            <Text fontWeight="semibold"
            textAlign="start"
            fontSize="lg">Username:</Text>
            <Input placeholder="Masukkan Username" size="lg"
            marginBottom={4}
            ref={usernameRef}
            type="text" />

            <Text fontWeight="semibold"
            textAlign="start"
            fontSize="lg">Password:</Text>
            <Input placeholder="Masukkan Password" size="lg"
            marginBottom={4}
            ref={passwordRef}
            type="password" />

            <Text fontWeight="semibold"
            textAlign="start"
            fontSize="lg">Jenis Pengguna:</Text>
            <Select placeholder="Pilih jenis pengguna" size="lg"
            marginBottom={4}
            ref={typeRef}>
                <option value="admin">Administrator</option>
                <option value="writer">Writer</option>
            </Select>

            <Button colorScheme="green"
            size="lg"
            isLoading={isCreateUsers}
            onClick={onDoCreate}>
                Simpan
            </Button>

        </Box>
    )
}

export default CreateUsers