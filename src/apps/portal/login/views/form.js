import React from "react"
import {
    Box,
    Container,
    Input,
    Button,
    Image,
    Text
} from "@chakra-ui/react"

import { useToast } from "@chakra-ui/toast"
import CryptoJS from "crypto-js"

import MainContext from "../../../../utils/provider"
import AuthAPI from "../../../../models/api/request/auth"

const FormView = () => {

    const [isDoLogin, setIsDoLogin] = React.useState(false)
    const toast = useToast()
    const unameRef = React.useRef()
    const passwRef = React.useRef()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    // On Do Login
    const onDoLogin = (provider) => {

        const username = unameRef.current.value
        const password = passwRef.current.value

        if (username === "") toastError("Gagal Masuk", "Username harus di isi")
        else if (password === "") toastError("Gagal Masuk", "Password harus di isi")
        else {
            setIsDoLogin(true)
            AuthAPI.login(username, password, res => {
                if (!res.success) toastError("Gagal Masuk", res.data.message)
                else {
                    if (res.data.length === 1) {
                        const data = res.data[0]
                        if (data.active_key === "" || data.active_key === null) {
                            onLoginSuccess(provider, data)
                        } else toastError("Gagal Masuk", "Akun sedang digunakan diperangkat lain")  
                    } else toastError("Gagal Masuk", "Akun tidak ditemukan")
                }
                console.log(res)
                window.setTimeout(() => setIsDoLogin(false), 1000)
            })
        }
    }

    // On Login Success
    const onLoginSuccess = (provider, data) => {
        const main_state = provider.main_state

        data.active_key = CryptoJS.MD5(new Date().getTime().toString()).toString()
        AuthAPI.setSession(data.id, data, _ => {
            window.localStorage.setItem(main_state.db, JSON.stringify(data))
            toastSuccess("Berhasil Masuk", "Mohon tunggu sebentar, sedang mengalihkan")
            window.setTimeout(() => window.location.replace("/"), 1000)
        })
    }

    // Setup Display
    const setupDisplay = (provider) => {
        const main_state = provider.main_state
        const base_url = main_state.base_url
        const header = main_state.header
        const login = main_state.sections.login.fields

        return (
            <Box alignItems="start">
                <center>
                    <Image src={base_url + header.logo.main} boxSize={36} />
                </center>
                <br />
                <Text fontSize="2xl" dangerouslySetInnerHTML={{
                    __html: header.title
                }}
                fontWeight="bold"
                fontFamily="sans-serif"
                textAlign="center" />
                <br />

                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">{login.username.label}:</Text>
                <Input placeholder={login.username.hint} size="lg"
                marginBottom={4}
                ref={unameRef}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">{login.password.label}:</Text>
                <Input placeholder={login.password.hint} size="lg" 
                marginBottom={4}
                ref={passwRef}
                type="password" />

                <Button isFullWidth={true} colorScheme="green"
                size="lg"
                isLoading={isDoLogin}
                onClick={() => onDoLogin(provider)}>
                    {login.submit.label}
                </Button>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                <Box paddingY={8}>
                    <Container maxW="container.sm"
                    bgColor="white"
                    padding={8}
                    border="1px solid rgba(0,0,0,0.1)">
                        
                        { setupDisplay(provider) }

                    </Container>
                </Box>
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default FormView