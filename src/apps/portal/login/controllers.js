import React from "react"

import MainContext from "../../../utils/provider"
import PortalUseCase from "../../../usecase/portal"

import FormView from "./views/form"

const LoginController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <PortalUseCase render={_ => (
                    <>
                        <FormView />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default LoginController