import React from "react"

import MainContext from "../../../utils/provider"
import DashboardUseCase from "../../../usecase/dashboard"

const ManageAccountsController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <DashboardUseCase render={_ => (
                    <>
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ManageAccountsController