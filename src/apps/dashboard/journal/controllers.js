import React from "react"

import MainContext from "../../../utils/provider"
import DashboardUseCase from "../../../usecase/dashboard"

import ListJournals from "./views/listJournals"

const ManageJournalController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <DashboardUseCase render={_ => (
                    <>
                        <ListJournals provider={provider} />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ManageJournalController