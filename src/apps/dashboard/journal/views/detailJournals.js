import React from "react"
import {
    Box,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    IconButton,
    Button,
    Text
} from "@chakra-ui/react"
import {
    InfoOutlineIcon
} from "@chakra-ui/icons"
import ReactMarkdown from "react-markdown"

import { useDisclosure } from "@chakra-ui/hooks"
import { ReqAPI } from "../../../../models/api/req"

const DetailJournals = ({provider, item}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()

    const setupDisplay = () => {
        let images = ReqAPI.base_url_image + "/" + item.file_name.replace("\\", "/")
        images = images.replace("/public", "")

        return (
            <Box>
                <Box
                    marginBottom={4}>
                    <Text fontSize="2xl" fontWeight="semibold">
                        {item.title}
                    </Text>
                </Box>
                <ReactMarkdown children={item.abstract} />
                <Button colorScheme="green"
                    marginY={4}
                    onClick={() => {
                        const fileName = images.split("/")
                        const link = document.createElement('a');
                        link.href = images;
                        link.setAttribute(
                            'download',
                            fileName[fileName.length - 1],
                        );
                        link.click();
                    }}
                    size="sm">Download Jurnal</Button>
            </Box>
        )
    }

    return (
        <>
            <IconButton size="sm" icon={<InfoOutlineIcon />}
                colorScheme="blue"
                onClick={onOpen} />
            <Modal onClose={onClose} size="4xl" isOpen={isOpen}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Detail Jurnal</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        { setupDisplay() }
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    )
}

export default DetailJournals