import React from "react"
import {
    Box,
    Flex,
    Text,
    Spacer,
    Center,
    Input,
    InputGroup,
    InputLeftElement,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Menu,
    MenuButton,
    MenuList,
    IconButton
} from "@chakra-ui/react"
import {
    Search2Icon,
    EditIcon
} from "@chakra-ui/icons"

import CreateJournals from "./createJournals"
import DetailJournals from "./detailJournals"
import DeleteJournals from "./deleteJournals"
import UpdateJournals from "./updateJournals"

import JournalsAPI from "../../../../models/api/request/journals"

const ListJournalsTable = ({provider, data=[], onSearch}) => {

    const searchRef = React.useRef();

    const onSuccess = () => {
        window.setTimeout(() => window.location.reload(), 1000)
    }

    const setupFilterTable = () => {

        return (
            <InputGroup
            variant="flushed">
                <InputLeftElement
                pointerEvents="none"
                children={<Search2Icon color="gray.700" />} />
                <Input type="search" placeholder="Cari judul, abstrak dan lainnya..."
                    textColor="gray.700"
                    ref={searchRef}
                    onChange={() => onSearch(searchRef.current.value)} />
            </InputGroup>
        )
    }

    const setupTableHead = () => {

        return (
            <Thead>
                <Tr>
                    <Th>Judul</Th>
                    <Th>Keywords</Th>
                    <Th>Tanggal</Th>
                    <Th colSpan={3}>Aksi</Th>
                </Tr>
            </Thead>
        )
    }

    const setupTableBody = () => {
        
        if (data.length === 0)
        return (
            <Tbody>
                <Tr>
                    <Td colSpan={6}>
                        <center>Belum ada data jurnal</center>
                    </Td>
                </Tr>
            </Tbody>
        )

        else return (
            <Tbody>
                { data.map((item, position) => {
                    return <Tr key={"item-list-news-" + position}>
                        <Td>{item.title}</Td>
                        <Td>{item.keywords}</Td>
                        <Td>{item.updated_at}</Td>
                        <Td paddingX={2}>
                            <DetailJournals provider={provider} item={item} />
                        </Td>
                        <Td paddingX={2}>
                            <Menu>
                                <MenuButton as={IconButton}
                                colorScheme="yellow"
                                size="sm"
                                icon={<EditIcon />} />
                                <MenuList>
                                    <UpdateJournals provider={provider}
                                        item={item}
                                        onSuccess={() => onSuccess()} />
                                </MenuList>
                            </Menu>
                        </Td>
                        <Td paddingX={2}>
                            <DeleteJournals provider={provider} item={item}
                                onSuccess={() => onSuccess()} />
                        </Td>
                    </Tr>
                }) }
            </Tbody>
        )
    }

    const setupTable = () => {

        return (
            <Table border="1px solid rgba(0,0,0,0.1)">
                { setupTableHead() }
                { setupTableBody() }
            </Table>
        )
    }

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">
            { setupFilterTable() }

            <br />
            { setupTable() }
        </Box>
    )
}

const ListJournals = ({provider}) => {
    const [dataList, setDataList] = React.useState([])

    React.useEffect(() => {
        loadDataList("")
    }, [])

    const loadDataList = (search) => {
        JournalsAPI.list(0, 10, search, item => {
            if (item.success) setDataList(item.data)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Flex paddingX={8} paddingY={4}>
                    <Text fontSize="4xl"
                        fontWeight="bold">Kelola Jurnal</Text>     
                    <Spacer />
                    <Center>
                        <CreateJournals provider={provider}
                        onSuccess={loadDataList} />
                    </Center>
                </Flex>

                <ListJournalsTable provider={provider}
                    data={dataList}
                    onSearch={(search) => loadDataList(search)} />
            </Box>
        )
    }

    return (
        <React.Fragment>
            { setupDisplay() }
        </React.Fragment>
    )
}

export default ListJournals