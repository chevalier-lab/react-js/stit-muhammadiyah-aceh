import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
} from "@chakra-ui/react"
import {
    EditIcon,
} from "@chakra-ui/icons"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import JournalsAPI from "../../../../models/api/request/journals"

const CreateJournals = ({provider, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [abstractRef, setAbstractRef] = React.useState('')
    const keywordsRef = React.useRef();
    const usersRef = React.useRef();
    const coverRef = React.useRef();
    const [isDoCreate, setIsDoCreate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitJournalsAPI = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Membuat", "Judul harap di isi")
            return
        }
        else if (abstractRef === "") {
            toastError("Gagal Membuat", "Konten harap di isi")
            return
        }
        else if (keywordsRef.current.value === "") {
            toastError("Gagal Membuat", "Tag harap di isi")
            return
        }
        else if (usersRef.current.value === "") {
            toastError("Gagal Membuat", "Authors harap di isi")
            return
        }
        else if (coverRef.current.files.length === 0) {
            toastError("Gagal Membuat", "Cover harap di pilih")
            return
        }

        setIsDoCreate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("keywords", keywordsRef.current.value)
        formData.append("authors", usersRef.current.value)
        formData.append("abstract", abstractRef)
        formData.append("cover", 
            coverRef.current.files[0],
            coverRef.current.files[0].name)

        JournalsAPI.create(provider.main_state.auth, formData, item => {
            if (item.success) {
                onClose()
                toastSuccess("Berhasil Membuat", item.message)

                setAbstractRef('')
                keywordsRef.current.value = ""
                usersRef.current.value = ""
                titleRef.current.value = ""
                coverRef.current.value = null

                onSuccess()
            } else toastError("Gagal Membuat", item.message)
            setIsDoCreate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Paper" size="md"
                marginBottom={4}
                ref={titleRef}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Abstrak:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={abstractRef}
                        handler={setAbstractRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Keywords:</Text>
                <Input placeholder="Masukkan Kata Kunci Jurnal" size="md"
                input="text"
                marginBottom={4}
                ref={keywordsRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Authors:</Text>
                <Input placeholder="Masukkan Nama Authors, Pisahkan dengan ," size="md"
                input="text"
                ref={usersRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">File Paper:</Text>
                <Input placeholder="Pilih Paper" size="md"
                type="file"
                marginBottom={4}
                ref={coverRef} />
            </Box>
        )
    }

    return (
        <>
            <Button onClick={onOpen}
                leftIcon={<EditIcon />}
                colorScheme="green">Unggah Jurnal</Button>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Unggah Jurnal Baru</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoCreate}
                            onClick={onSubmitJournalsAPI}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default CreateJournals