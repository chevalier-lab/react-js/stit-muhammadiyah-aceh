import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
    MenuItem
} from "@chakra-ui/react"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import JournalsAPI from "../../../../models/api/request/journals"

const UpdateAnnouncements = ({provider, item, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [abstractRef, setAbstractRef] = React.useState(item.abstract)
    const keywordsRef = React.useRef();
    const usersRef = React.useRef();
    const [isDoUpdate, setIsDoUpdate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitJournals = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Mengubah", "Judul harap di isi")
            return
        }
        else if (abstractRef === "") {
            toastError("Gagal Mengubah", "Abstrak harap di isi")
            return
        }
        else if (keywordsRef.current.value === "") {
            toastError("Gagal Mengubah", "Keywords harap di isi")
            return
        }
        else if (usersRef.current.value === "") {
            toastError("Gagal Mengubah", "Auhtors harap di isi")
            return
        }

        setIsDoUpdate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("keywords", keywordsRef.current.value)
        formData.append("authors", usersRef.current.value)
        formData.append("abstract", abstractRef)

        JournalsAPI.update(provider.main_state.auth, item.id, formData, res => {
            if (res.success) {
                onClose()
                toastSuccess("Berhasil Mengubah", res.message)

                setAbstractRef('')
                keywordsRef.current.value = ""
                usersRef.current.value = ""
                titleRef.current.value = ""

                onSuccess()
            } else toastError("Gagal Mengubah", res.message)
            setIsDoUpdate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Pengumuman" size="md"
                marginBottom={4}
                ref={titleRef}
                defaultValue={item.title}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={abstractRef}
                        handler={setAbstractRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Keywords:</Text>
                <Input placeholder="Masukkan Kata Kunci Jurnal" size="md"
                marginBottom={4}
                defaultValue={item.keywords}
                input="text"
                ref={keywordsRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Authors:</Text>
                <Input placeholder="Masukkan Author, pisahkan dengan ," size="md"
                marginBottom={4}
                defaultValue={item.authors}
                input="text"
                ref={usersRef} />
            </Box>
        )
    }

    return (
        <>
            <MenuItem onClick={onOpen}>Ubah Jurnal</MenuItem>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Ubah Jurnal</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoUpdate}
                            onClick={onSubmitJournals}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default UpdateAnnouncements