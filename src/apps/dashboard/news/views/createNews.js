import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
    Flex,
    IconButton,
    Select
} from "@chakra-ui/react"
import {
    EditIcon,
    AddIcon
} from "@chakra-ui/icons"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import NewsAPI from "../../../../models/api/request/news"

import IndexCategories from "../../../../components/categories"

const SetupCategories = ({provider, onCloseModal}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    
    return (
        <>
            <IconButton icon={<AddIcon />}
                onClick={() => {
                    onOpen()
                }}
                colorScheme="green" />
            <IndexCategories provider={provider} isOpen={isOpen}
                onClose={() => {
                    onClose()
                    onCloseModal()
                }} />
        </>
    )
}

const CreateNews = ({provider, categories, onSuccess, onLoadCategories}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [contentRef, setContentRef] = React.useState('')
    const categoryRef = React.useRef();
    const tagsRef = React.useRef();
    const coverRef = React.useRef();
    const [isDoCreate, setIsDoCreate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitNews = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Membuat", "Judul harap di isi")
            return
        }
        else if (categoryRef.current.value === "") {
            toastError("Gagal Membuat", "Kategori harap di isi")
            return
        }
        else if (contentRef === "") {
            toastError("Gagal Membuat", "Konten harap di isi")
            return
        }
        else if (tagsRef.current.value === "") {
            toastError("Gagal Membuat", "Tag harap di isi")
            return
        }
        else if (coverRef.current.files.length === 0) {
            toastError("Gagal Membuat", "Cover harap di pilih")
            return
        }

        setIsDoCreate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("categoryId", categoryRef.current.value)
        formData.append("tags", tagsRef.current.value)
        formData.append("content", contentRef)
        formData.append("cover", 
            coverRef.current.files[0],
            coverRef.current.files[0].name)

        NewsAPI.create(provider.main_state.auth, formData, item => {
            if (item.success) {
                onClose()
                toastSuccess("Berhasil Membuat", item.message)

                setContentRef('')
                tagsRef.current.value = ""
                categoryRef.current.value = ""
                titleRef.current.value = ""
                coverRef.current.value = null

                onSuccess()
            } else toastError("Gagal Membuat", item.message)
            setIsDoCreate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Berita" size="md"
                marginBottom={4}
                ref={titleRef}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={contentRef}
                        handler={setContentRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Kategori:</Text>
                <Flex
                    marginBottom={4}>
                    <SetupCategories provider={provider}
                    onCloseModal={onLoadCategories} />
                    <Select placeholder="Pilih Kategori" size="md"
                        marginStart={4}
                    ref={categoryRef}>
                        { categories.map(item => {
                            return <option value={item.id}
                                key={"categories-add-" + item.id}>{item.label}</option>
                        }) }
                    </Select>
                </Flex>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Tags:</Text>
                <Input placeholder="Masukkan Kata Kunci Berita" size="md"
                input="text"
                marginBottom={4}
                ref={tagsRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Cover:</Text>
                <Input placeholder="Pilih Cover Berita" size="md"
                type="file"
                marginBottom={4}
                ref={coverRef} />
            </Box>
        )
    }

    return (
        <>
            <Button onClick={onOpen}
                leftIcon={<EditIcon />}
                colorScheme="green">Tulis Berita</Button>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Tulis Berita Baru</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoCreate}
                            onClick={onSubmitNews}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default CreateNews