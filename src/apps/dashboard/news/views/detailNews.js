import React from "react"
import {
    Box,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    IconButton,
    Image,
    Text
} from "@chakra-ui/react"
import {
    InfoOutlineIcon
} from "@chakra-ui/icons"
import ReactMarkdown from "react-markdown"

import { useDisclosure } from "@chakra-ui/hooks"
import { ReqAPI } from "../../../../models/api/req"

const DetailNews = ({provider, item}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()

    const setupDisplay = () => {
        let images = ReqAPI.base_url_image + "/" + item.file_name.replace("\\", "/")
        images = images.replace("/public", "")

        return (
            <Box>
                <Image src={images} 
                    alt={images}
                    maxW={"100%"} />
                <Box
                    marginY={4}>
                    <Text fontSize="2xl" fontWeight="semibold">
                        {item.title}
                    </Text>
                </Box>
                <ReactMarkdown children={item.content} />
            </Box>
        )
    }

    return (
        <>
            <IconButton size="sm" icon={<InfoOutlineIcon />}
                colorScheme="blue"
                onClick={onOpen} />
            <Modal onClose={onClose} size="4xl" isOpen={isOpen}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Detail Berita</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        { setupDisplay() }
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    )
}

export default DetailNews