import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
    Select,
    MenuItem
} from "@chakra-ui/react"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import NewsAPI from "../../../../models/api/request/news"

const UpdateNews = ({provider, categories, item, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [contentRef, setContentRef] = React.useState(item.content)
    const categoryRef = React.useRef();
    const tagsRef = React.useRef();
    const [isDoUpdate, setIsDoUpdate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitNews = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Mengubah", "Judul harap di isi")
            return
        }
        else if (categoryRef.current.value === "") {
            toastError("Gagal Mengubah", "Kategori harap di isi")
            return
        }
        else if (contentRef === "") {
            toastError("Gagal Mengubah", "Konten harap di isi")
            return
        }
        else if (tagsRef.current.value === "") {
            toastError("Gagal Mengubah", "Tag harap di isi")
            return
        }

        setIsDoUpdate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("categoryId", categoryRef.current.value)
        formData.append("tags", tagsRef.current.value)
        formData.append("content", contentRef)

        NewsAPI.update(provider.main_state.auth, item.id, formData, res => {
            if (res.success) {
                onClose()
                toastSuccess("Berhasil Mengubah", res.message)

                setContentRef('')
                tagsRef.current.value = ""
                categoryRef.current.value = ""
                titleRef.current.value = ""

                onSuccess()
            } else toastError("Gagal Mengubah", res.message)
            setIsDoUpdate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Berita" size="md"
                marginBottom={4}
                ref={titleRef}
                defaultValue={item.title}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={contentRef}
                        handler={setContentRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Kategori:</Text>
                <Select placeholder="Pilih Kategori" size="md"
                marginBottom={4}
                defaultValue={item.categoryId}
                ref={categoryRef}>
                    { categories.map(item => {
                        return <option value={item.id}
                            key={"categories-add-" + item.id}>{item.label}</option>
                    }) }
                </Select>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Tags:</Text>
                <Input placeholder="Masukkan Kata Kunci Berita" size="md"
                marginBottom={4}
                defaultValue={item.tags}
                input="text"
                ref={tagsRef} />
            </Box>
        )
    }

    return (
        <>
            <MenuItem onClick={onOpen}>Ubah Berita</MenuItem>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Ubah Berita</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoUpdate}
                            onClick={onSubmitNews}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default UpdateNews