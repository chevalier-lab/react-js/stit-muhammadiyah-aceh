import React from "react"
import {
    Box,
    Flex,
    Text,
    Spacer,
    Center,
    Input,
    InputGroup,
    InputLeftElement,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Menu,
    MenuButton,
    MenuList,
    IconButton
} from "@chakra-ui/react"
import {
    Search2Icon,
    EditIcon
} from "@chakra-ui/icons"

import CreateNews from "./createNews"
import DetailNews from "./detailNews"
import DeleteNews from "./deleteNews"
import UpdateNews from "./updateNews"

import CategoriesAPI from "../../../../models/api/request/categories"
import NewsAPI from "../../../../models/api/request/news"

const ListNewsTable = ({provider, categories, data=[], onSearch}) => {

    const searchRef = React.useRef();

    const onSuccess = () => {
        window.setTimeout(() => window.location.reload(), 1000)
    }

    const setupFilterTable = () => {

        return (
            <InputGroup
            variant="flushed">
                <InputLeftElement
                pointerEvents="none"
                children={<Search2Icon color="gray.700" />} />
                <Input type="search" placeholder="Cari judul, deskripsi dan lainnya..."
                    textColor="gray.700"
                    ref={searchRef}
                    onChange={() => onSearch(searchRef.current.value)} />
            </InputGroup>
        )
    }

    const setupTableHead = () => {

        return (
            <Thead>
                <Tr>
                    <Th>Judul</Th>
                    <Th>Author</Th>
                    <Th>Kategori</Th>
                    <Th>Tags</Th>
                    <Th>Tanggal</Th>
                    <Th colSpan={3}>Aksi</Th>
                </Tr>
            </Thead>
        )
    }

    const setupTableBody = () => {
        
        if (data.length === 0)
        return (
            <Tbody>
                <Tr>
                    <Td colSpan={8}>
                        <center>Belum ada data berita</center>
                    </Td>
                </Tr>
            </Tbody>
        )

        else return (
            <Tbody>
                { data.map((item, position) => {
                    return <Tr key={"item-list-news-" + position}>
                        <Td>{item.title}</Td>
                        <Td>{item.username}</Td>
                        <Td>{item.label}</Td>
                        <Td>{item.tags}</Td>
                        <Td>{item.updated_at}</Td>
                        <Td paddingX={2}>
                            <DetailNews provider={provider} item={item} />
                        </Td>
                        <Td paddingX={2}>
                            <Menu>
                                <MenuButton as={IconButton}
                                colorScheme="yellow"
                                size="sm"
                                icon={<EditIcon />} />
                                <MenuList>
                                    <UpdateNews provider={provider}
                                        categories={categories}
                                        item={item}
                                        onSuccess={() => onSuccess()} />
                                </MenuList>
                            </Menu>
                        </Td>
                        <Td paddingX={2}>
                            <DeleteNews provider={provider} item={item}
                                onSuccess={() => onSuccess()} />
                        </Td>
                    </Tr>
                }) }
            </Tbody>
        )
    }

    const setupTable = () => {

        return (
            <Table border="1px solid rgba(0,0,0,0.1)">
                { setupTableHead() }
                { setupTableBody() }
            </Table>
        )
    }

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">
            { setupFilterTable() }

            <br />
            { setupTable() }
        </Box>
    )
}

const ListNews = ({provider}) => {
    const [categories, setCategories] = React.useState([])
    const [dataList, setDataList] = React.useState([])

    React.useEffect(() => {
        loadDataCategories()

        loadDataList("")
    }, [])

    const loadDataCategories = () => {
        CategoriesAPI.list(item => {
            if (item.success) setCategories(item.data)
        })
    }

    const loadDataList = (search) => {
        NewsAPI.list(0, 10, search, item => {
            if (item.success) setDataList(item.data)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Flex paddingX={8} paddingY={4}>
                    <Text fontSize="4xl"
                        fontWeight="bold">Kelola Berita</Text>     
                    <Spacer />
                    <Center>
                        <CreateNews provider={provider}
                        categories={categories}
                        onSuccess={loadDataList}
                        onLoadCategories={loadDataCategories} />
                    </Center>
                </Flex>

                <ListNewsTable provider={provider}
                    categories={categories}
                    data={dataList}
                    onSearch={(search) => loadDataList(search)} />
            </Box>
        )
    }

    return (
        <React.Fragment>
            { setupDisplay() }
        </React.Fragment>
    )
}

export default ListNews