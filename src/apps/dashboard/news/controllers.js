import React from "react"

import MainContext from "../../../utils/provider"
import DashboardUseCase from "../../../usecase/dashboard"

import ListNews from "./views/listNews"

const ManageNewsController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <DashboardUseCase render={_ => (
                    <>
                        <ListNews provider={provider} />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ManageNewsController