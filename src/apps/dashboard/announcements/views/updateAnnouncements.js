import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
    MenuItem
} from "@chakra-ui/react"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import AnnouncementsAPI from "../../../../models/api/request/announcements"

const UpdateAnnouncements = ({provider, item, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [contentRef, setContentRef] = React.useState(item.content)
    const tagsRef = React.useRef();
    const [isDoUpdate, setIsDoUpdate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitAnnouncements = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Mengubah", "Judul harap di isi")
            return
        }
        else if (contentRef === "") {
            toastError("Gagal Mengubah", "Konten harap di isi")
            return
        }
        else if (tagsRef.current.value === "") {
            toastError("Gagal Mengubah", "Tag harap di isi")
            return
        }

        setIsDoUpdate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("tags", tagsRef.current.value)
        formData.append("content", contentRef)

        AnnouncementsAPI.update(provider.main_state.auth, item.id, formData, res => {
            if (res.success) {
                onClose()
                toastSuccess("Berhasil Mengubah", res.message)

                setContentRef('')
                tagsRef.current.value = ""
                titleRef.current.value = ""

                onSuccess()
            } else toastError("Gagal Mengubah", res.message)
            setIsDoUpdate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Pengumuman" size="md"
                marginBottom={4}
                ref={titleRef}
                defaultValue={item.title}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={contentRef}
                        handler={setContentRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Tags:</Text>
                <Input placeholder="Masukkan Kata Kunci Pengumuman" size="md"
                marginBottom={4}
                defaultValue={item.tags}
                input="text"
                ref={tagsRef} />
            </Box>
        )
    }

    return (
        <>
            <MenuItem onClick={onOpen}>Ubah Pengumuman</MenuItem>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Ubah Pengumuman</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoUpdate}
                            onClick={onSubmitAnnouncements}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default UpdateAnnouncements