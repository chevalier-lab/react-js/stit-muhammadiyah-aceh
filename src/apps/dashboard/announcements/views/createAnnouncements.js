import React from "react"
import {
    Button,
    Drawer,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    DrawerHeader,
    DrawerBody,
    DrawerFooter,
    Input,
    Text,
    Box,
} from "@chakra-ui/react"
import {
    EditIcon,
} from "@chakra-ui/icons"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import AnnouncementsAPI from "../../../../models/api/request/announcements"

const CreateAnnouncements = ({provider, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [contentRef, setContentRef] = React.useState('')
    const tagsRef = React.useRef();
    const coverRef = React.useRef();
    const [isDoCreate, setIsDoCreate] = React.useState(false) 

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const onSubmitAnnouncementsAPI = () => {

        if (titleRef.current.value === "") {
            toastError("Gagal Membuat", "Judul harap di isi")
            return
        }
        else if (contentRef === "") {
            toastError("Gagal Membuat", "Konten harap di isi")
            return
        }
        else if (tagsRef.current.value === "") {
            toastError("Gagal Membuat", "Tag harap di isi")
            return
        }
        else if (coverRef.current.files.length === 0) {
            toastError("Gagal Membuat", "Cover harap di pilih")
            return
        }

        setIsDoCreate(true)
        const formData = new FormData()
        formData.append("title", titleRef.current.value)
        formData.append("tags", tagsRef.current.value)
        formData.append("content", contentRef)
        formData.append("cover", 
            coverRef.current.files[0],
            coverRef.current.files[0].name)

        AnnouncementsAPI.create(provider.main_state.auth, formData, item => {
            if (item.success) {
                onClose()
                toastSuccess("Berhasil Membuat", item.message)

                setContentRef('')
                tagsRef.current.value = ""
                titleRef.current.value = ""
                coverRef.current.value = null

                onSuccess()
            } else toastError("Gagal Membuat", item.message)
            setIsDoCreate(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul Pengumuman" size="md"
                marginBottom={4}
                ref={titleRef}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={contentRef}
                        handler={setContentRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Tags:</Text>
                <Input placeholder="Masukkan Kata Kunci Pengumuman" size="md"
                input="text"
                marginBottom={4}
                ref={tagsRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Cover:</Text>
                <Input placeholder="Pilih Cover Pengumuman" size="md"
                type="file"
                marginBottom={4}
                ref={coverRef} />
            </Box>
        )
    }

    return (
        <>
            <Button onClick={onOpen}
                leftIcon={<EditIcon />}
                colorScheme="green">Tulis Pengumuman</Button>

            <Drawer isOpen={isOpen} onClose={onClose}
                size="xl">
                <DrawerOverlay />
                <DrawerContent>
                    <DrawerCloseButton />
                    <DrawerHeader>Tulis Pengumuman Baru</DrawerHeader>
                    
                    <DrawerBody
                        overflowY="auto">
                        { setupDisplay() }
                    </DrawerBody>

                    <DrawerFooter>
                        <Button colorScheme="green"
                            isFullWidth={true}
                            isLoading={isDoCreate}
                            onClick={onSubmitAnnouncementsAPI}>
                            Simpan
                        </Button>
                    </DrawerFooter>
                </DrawerContent>
            </Drawer>
        </>
    )
}

export default CreateAnnouncements