import React from "react"
import {
    Box,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    ModalFooter,
    IconButton,
    Text,
    Button
} from "@chakra-ui/react"
import {
    DeleteIcon
} from "@chakra-ui/icons"
import { useToast } from "@chakra-ui/toast"

import { useDisclosure } from "@chakra-ui/hooks"

import AnnouncementsAPI from "../../../../models/api/request/announcements"

const DeleteAnnouncements = ({provider, item, onSuccess}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [isDoDelete, setIsDoDelete] = React.useState(false)
    const toast = useToast()

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    const doDelete = () => {
        setIsDoDelete(true)
        AnnouncementsAPI.delete(item.id, res => {
            if (res.success) {
                toastSuccess("Berhasil Menghapus", item.message)
                onClose()
                onSuccess()
            } else toastError("Gagal Menghapus", item.message)

            setIsDoDelete(false)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Text>
                    Apa anda yakin ingin menghapus <strong>{item.title}</strong>?
                </Text>
            </Box>
        )
    }

    return (
        <>
        <IconButton size="sm" icon={<DeleteIcon />}
            colorScheme="red"
            onClick={onOpen} />
        <Modal onClose={onClose} size="md" isOpen={isOpen}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Hapus Pengumuman</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    { setupDisplay() }
                </ModalBody>
                <ModalFooter>
                    <Button onClick={doDelete}
                        isLoading={isDoDelete}
                        colorScheme="red">Hapus</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
        </>
    )
}

export default DeleteAnnouncements