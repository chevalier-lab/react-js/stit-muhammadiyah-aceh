import React from "react"
import {
    Box,
    Flex,
    Text,
    Spacer,
    Center,
    Input,
    InputGroup,
    InputLeftElement,
    Table,
    Thead,
    Tbody,
    Tr,
    Td,
    Th,
    Menu,
    MenuButton,
    MenuList,
    IconButton
} from "@chakra-ui/react"
import {
    Search2Icon,
    EditIcon
} from "@chakra-ui/icons"

import CreateAnnouncements from "./createAnnouncements"
import DetailAnnouncements from "./detailAnnouncements"
import DeleteAnnouncements from "./deleteAnnouncements"
import UpdateAnnouncements from "./updateAnnouncements"

import AnnouncementsAPI from "../../../../models/api/request/announcements"

const ListAnnouncementsTable = ({provider, data=[], onSearch}) => {

    const searchRef = React.useRef();

    const onSuccess = () => {
        window.setTimeout(() => window.location.reload(), 1000)
    }

    const setupFilterTable = () => {

        return (
            <InputGroup
            variant="flushed">
                <InputLeftElement
                pointerEvents="none"
                children={<Search2Icon color="gray.700" />} />
                <Input type="search" placeholder="Cari judul, deskripsi dan lainnya..."
                    textColor="gray.700"
                    ref={searchRef}
                    onChange={() => onSearch(searchRef.current.value)} />
            </InputGroup>
        )
    }

    const setupTableHead = () => {

        return (
            <Thead>
                <Tr>
                    <Th>Judul</Th>
                    <Th>Author</Th>
                    <Th>Tags</Th>
                    <Th>Tanggal</Th>
                    <Th colSpan={3}>Aksi</Th>
                </Tr>
            </Thead>
        )
    }

    const setupTableBody = () => {
        
        if (data.length === 0)
        return (
            <Tbody>
                <Tr>
                    <Td colSpan={7}>
                        <center>Belum ada data pengumuman</center>
                    </Td>
                </Tr>
            </Tbody>
        )

        else return (
            <Tbody>
                { data.map((item, position) => {
                    return <Tr key={"item-list-news-" + position}>
                        <Td>{item.title}</Td>
                        <Td>{item.username}</Td>
                        <Td>{item.tags}</Td>
                        <Td>{item.updated_at}</Td>
                        <Td paddingX={2}>
                            <DetailAnnouncements provider={provider} item={item} />
                        </Td>
                        <Td paddingX={2}>
                            <Menu>
                                <MenuButton as={IconButton}
                                colorScheme="yellow"
                                size="sm"
                                icon={<EditIcon />} />
                                <MenuList>
                                    <UpdateAnnouncements provider={provider}
                                        item={item}
                                        onSuccess={() => onSuccess()} />
                                </MenuList>
                            </Menu>
                        </Td>
                        <Td paddingX={2}>
                            <DeleteAnnouncements provider={provider} item={item}
                                onSuccess={() => onSuccess()} />
                        </Td>
                    </Tr>
                }) }
            </Tbody>
        )
    }

    const setupTable = () => {

        return (
            <Table border="1px solid rgba(0,0,0,0.1)">
                { setupTableHead() }
                { setupTableBody() }
            </Table>
        )
    }

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">
            { setupFilterTable() }

            <br />
            { setupTable() }
        </Box>
    )
}

const ListAnnouncements = ({provider}) => {
    const [dataList, setDataList] = React.useState([])

    React.useEffect(() => {
        loadDataList("")
    }, [])

    const loadDataList = (search) => {
        AnnouncementsAPI.list(0, 10, search, item => {
            if (item.success) setDataList(item.data)
        })
    }

    const setupDisplay = () => {

        return (
            <Box>
                <Flex paddingX={8} paddingY={4}>
                    <Text fontSize="4xl"
                        fontWeight="bold">Kelola Pengumuman</Text>     
                    <Spacer />
                    <Center>
                        <CreateAnnouncements provider={provider}
                        onSuccess={loadDataList} />
                    </Center>
                </Flex>

                <ListAnnouncementsTable provider={provider}
                    data={dataList}
                    onSearch={(search) => loadDataList(search)} />
            </Box>
        )
    }

    return (
        <React.Fragment>
            { setupDisplay() }
        </React.Fragment>
    )
}

export default ListAnnouncements