import React from "react"

import MainContext from "../../../utils/provider"
import DashboardUseCase from "../../../usecase/dashboard"

import ListAnnouncements from "./views/listAnnouncements"

const ManageAnnouncementsController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <DashboardUseCase render={_ => (
                    <>
                        <ListAnnouncements provider={provider} />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ManageAnnouncementsController