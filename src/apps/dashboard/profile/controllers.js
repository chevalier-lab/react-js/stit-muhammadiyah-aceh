import React from "react"

import MainContext from "../../../utils/provider"
import DashboardUseCase from "../../../usecase/dashboard"

import Quick from "./views/quick"

const ManageProfileController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <DashboardUseCase render={_ => (
                    <>
                        <Quick provider={provider} />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ManageProfileController