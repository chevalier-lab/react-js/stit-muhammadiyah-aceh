import React from "react"
import {
    Box,
    Text,
    Flex,
    Grid,
    GridItem,
    List,
    ListItem,
    ListIcon,
    UnorderedList,
    Spacer,
    Collapse
} from "@chakra-ui/react"
import {
    EditIcon,
    InfoOutlineIcon,
    AddIcon,
    StarIcon,
    UpDownIcon
} from "@chakra-ui/icons"

import { useDisclosure } from "@chakra-ui/hooks"

import IndexCategories from "../../../../components/categories"
import IndexUsers from "../../../../components/users"
import Customize from "./customize"

import LogsAPI from "../../../../models/api/request/logs"
import NewsAPI from "../../../../models/api/request/news"
import AnnouncementsAPI from "../../../../models/api/request/announcements"
import JournalsAPI from "../../../../models/api/request/journals"
import UsersAPI from "../../../../models/api/request/users"

const SetupCategories = ({provider}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    
    return (
        <>
            <ListItem
                onClick={onOpen}
                cursor="pointer">
                <ListIcon as={StarIcon} color="green.500" />
                Kelola Kategori
            </ListItem>
            <IndexCategories provider={provider} isOpen={isOpen}
                onClose={onClose} />
        </>
    )
}

const SetupUsers = ({provider}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    
    return (
        <>
            <ListItem
                onClick={onOpen}
                cursor="pointer">
                <ListIcon as={StarIcon} color="green.500" />
                Kelola Pengguna
            </ListItem>
            <IndexUsers provider={provider} isOpen={isOpen}
                onClose={onClose} />
        </>
    )
}

const QuickActionBox = ({provider}) => {

    const setupGetStarted = () => {

        return (
            <GridItem>
                <Text fontSize="xl"
                fontWeight="semibold"
                paddingBottom={4}>Memulai</Text>
                <Customize provider={provider} />
            </GridItem>
        )
    }

    const setupNextSteps = () => {

        return (
            <GridItem>
                <Text fontSize="xl"
                fontWeight="semibold"
                paddingBottom={2}>Tahapan Selanjutnya</Text>
                <List spacing={2}>
                    <ListItem
                        cursor="pointer"
                        onClick={() => {
                            window.location.replace("/manage-news")
                        }}>
                        <ListIcon as={EditIcon} color="green.500" />
                        Tulis Berita
                    </ListItem>
                    <ListItem
                        cursor="pointer"
                        onClick={() => {
                            window.location.replace("/manage-announcements")
                        }}>
                        <ListIcon as={InfoOutlineIcon} color="green.500" />
                        Tambahkan Pengumuman
                    </ListItem>
                    <ListItem
                        cursor="pointer"
                        onClick={() => {
                            window.location.replace("/manage-journal")
                        }}>
                        <ListIcon as={AddIcon} color="green.500" />
                        Unggah Jurnal
                    </ListItem>
                </List>
            </GridItem>
        )
    }

    const setupMasterData = () => {

        return (
            <GridItem>
                <Text fontSize="xl"
                fontWeight="semibold"
                paddingBottom={2}>Master Data</Text>
                <List spacing={2}>
                    <SetupCategories provider={provider} />
                    <SetupUsers provider={provider} />
                </List>
            </GridItem>
        )
    }

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">
            
            <Text fontSize="2xl"
                fontWeight="semibold">Welcome Back</Text>
            <Text fontSize="lg">Mulai kelolah website hari ini.</Text>

            <Grid templateColumns="repeat(3, 1fr)" gap={6}
                marginTop={4}>
                { setupGetStarted() }
                { setupNextSteps() }
                { setupMasterData() }
            </Grid>

        </Box>
    )
}

const QuickAnalyticsBox = ({provider}) => {
    const { isOpen, onToggle } = useDisclosure()
    const [dataCount, setDataCount] = React.useState({
        news: 0,
        journals: 0,
        announcements: 0,
        users: 0
    })

    const loadCount = () => {
        NewsAPI.count(res => {
            const temp = dataCount
            temp.news = res.data[0].total
            setDataCount(temp)
        })
        AnnouncementsAPI.count(res => {
            const temp = dataCount
            temp.announcements = res.data[0].total
            setDataCount(temp)
        })
        UsersAPI.count(res => {
            const temp = dataCount
            temp.users = res.data[0].total
            setDataCount(temp)
        })
        JournalsAPI.count(res => {
            const temp = dataCount
            temp.journals = res.data[0].total
            setDataCount(temp)
        })
    }

    React.useEffect(() => {
        loadCount()
    }, [])

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">

            <Flex
                onClick={onToggle}
                paddingBottom={4}
                borderBottom="1px solid rgba(0,0,0,0.1)">
                <Text fontSize="xl"
                fontWeight="semibold">Data Analisis</Text>
                <Spacer />
                <Box>
                    <UpDownIcon />
                </Box>
            </Flex>

            <Collapse in={isOpen} animateOpacity>
               <UnorderedList
                    paddingTop={4}>
                    <ListItem>
                        <strong>{dataCount.news}</strong> Berita
                    </ListItem>
                    <ListItem>
                        <strong>{dataCount.announcements}</strong> Pengumuman
                    </ListItem>
                    <ListItem>
                        <strong>{dataCount.journals}</strong> Jurnal
                    </ListItem>
                    <ListItem>
                        <strong>{dataCount.users}</strong> Pengguna
                    </ListItem>
                </UnorderedList>
            </Collapse>

        </Box>
    )
}

const QuickActivityBox = ({provider}) => {
    const { isOpen, onToggle } = useDisclosure()
    const [dataList, setDataList] = React.useState([])

    React.useEffect(() => {
        loadDataLogs()
    }, [])

    const loadDataLogs = () => {
        LogsAPI.list(0, 10, item => {
            if (item.success) setDataList(item.data)
        })
    }

    return (
        <Box padding={4}
            marginX={8}
            bgColor="white"
            border="1px solid rgba(0,0,0,0.1)">

            <Flex
                onClick={onToggle}
                paddingBottom={4}
                borderBottom="1px solid rgba(0,0,0,0.1)">
                <Text fontSize="xl"
                fontWeight="semibold">Data Aktifitas</Text>
                <Spacer />
                <Box>
                    <UpDownIcon />
                </Box>
            </Flex>

            <Collapse in={isOpen} animateOpacity>
                <UnorderedList
                    paddingTop={4}>
                    {dataList.map((item, position) => {
                        return <ListItem key={"logs-activity-" + position}>
                            <strong>{item.ip_address}</strong>, {item.activity}
                        </ListItem>
                    })}
                </UnorderedList>
            </Collapse>

        </Box>
    )
}

const Quick = ({provider}) => {

    const setupDisplay = () => {

        return (
            <Box>
                <Flex paddingX={8} paddingY={4}>
                    <Text fontSize="4xl"
                        fontWeight="bold">Dashboard</Text>        
                </Flex>

                <QuickActionBox provider={provider} />
                <Grid templateColumns="repeat(2, 1fr)" gap={6}
                marginTop={4}>
                    <GridItem>
                        <QuickAnalyticsBox provider={provider} />
                    </GridItem>
                    <GridItem>
                        <QuickActivityBox provider={provider} />
                    </GridItem>
                </Grid>
            </Box>
        )
    }

    return <React.Fragment>
        { setupDisplay() }
    </React.Fragment>
}

export default Quick