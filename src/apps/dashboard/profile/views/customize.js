import React from "react"
import {
    Box,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalCloseButton,
    ModalBody,
    Input,
    CircularProgress,
    Button,
    Text,
    ModalFooter
} from "@chakra-ui/react"
import YamdeComp from "yamde"
import { useToast } from "@chakra-ui/toast"
import {
    WarningIcon
} from "@chakra-ui/icons"

import { useDisclosure } from "@chakra-ui/hooks"
import HomeAPI from "../../../../models/api/request/home"

const Customize = ({provider}) => {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const toast = useToast()
    const titleRef = React.useRef();
    const [contentRef, setContentRef] = React.useState('')
    const addressRef = React.useRef();
    const phoneRef = React.useRef();
    const copyrightRef = React.useRef();
    const coverRef = React.useRef();
    const [isDoUpdate, setIsDoUpdate] = React.useState(false) 
    const [isLoadData, setIsLoadData] = React.useState(false) 
    const [data, setData] = React.useState(null)

    // On Toast Error
    const toastError = (title="", description="") => toast({
        title: title,
        description: description,
        status: "error",
        duration: 2000,
        isClosable: true,
      })
    
    // On Toast Success
    const toastSuccess = (title="", description="") => toast({
        title: title,
        description: description,
        status: "success",
        duration: 2000,
        isClosable: true,
      })

    React.useEffect(() => {
        HomeAPI.get(res => {
            if (res.success) {
                console.log(res.data)
                setData(res.data[0])
                setIsLoadData(true)
                setContentRef(res.data[0].content)
            }
        })
    }, [])

    const onSubmit = () => {
        const formData = new FormData()

        if (titleRef.current.value === "") {
            toastError("Gagal Menyimpan", "Judul harap di isi")
            return
        }
        else if (addressRef.current.value === "") {
            toastError("Gagal Menyimpan", "Alamat harap di isi")
            return
        }
        else if (contentRef === "") {
            toastError("Gagal Menyimpan", "Konten harap di isi")
            return
        }
        else if (phoneRef.current.value === "") {
            toastError("Gagal Menyimpan", "Nomor Telepon harap di isi")
            return
        }
        else if (copyrightRef.current.value === "") {
            toastError("Gagal Menyimpan", "Copyright harap di isi")
            return
        }

        setIsDoUpdate(true)
        formData.append("title", titleRef.current.value)
        formData.append("address", addressRef.current.value)
        formData.append("phone_number", phoneRef.current.value)
        formData.append("copyright", copyrightRef.current.value)
        formData.append("content", contentRef)

        HomeAPI.update(formData, res => {
            if (res.success) {
                onClose()
                toastSuccess("Berhasil Menyimpan", res.message)
                window.setTimeout(() => window.location.reload(), 1000)
            } else toastError("Gagal Menyimpan", res.message)
            setIsDoUpdate(false)
        })
    }

    const onSubmitBanner = () => {
        const formData = new FormData()

        setIsDoUpdate(true)

        if (coverRef.current.files.length > 0) {
            formData.append("cover", 
                coverRef.current.files[0],
                coverRef.current.files[0].name)
        }

        HomeAPI.banner(formData, res => {
            if (res.success) {
                onClose()
                toastSuccess("Berhasil Menyimpan", res.message)
                window.setTimeout(() => window.location.reload(), 1000)
            } else toastError("Gagal Menyimpan", res.message)
            setIsDoUpdate(false)
        })
    }

    const setupDisplay = () => {
        if (!isLoadData) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat data</Text>
            </Box>
        )

        if (data === null) return (
            <Box alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Gagal memuat data</Text>
            </Box>
        )

        return (
            <Box>
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Banner:</Text>
                <Input placeholder="Pilih Banner" size="md"
                type="file"
                marginBottom={4}
                onChange={onSubmitBanner}
                ref={coverRef} />

                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Judul:</Text>
                <Input placeholder="Masukkan Judul" size="md"
                marginBottom={4}
                ref={titleRef}
                defaultValue={data.title}
                type="text" />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Konten:</Text>
                <Box marginBottom={4}>
                    <YamdeComp value={contentRef}
                        handler={setContentRef}
                        theme="light" />
                </Box>
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Alamat:</Text>
                <Input placeholder="Masukkan Alamat" size="md"
                input="text"
                marginBottom={4}
                defaultValue={data.address}
                ref={addressRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Nomor Telepon:</Text>
                <Input placeholder="Masukkan Nomor Telepon" size="md"
                input="text"
                marginBottom={4}
                defaultValue={data.phone_number}
                ref={phoneRef} />
                
                <Text fontWeight="semibold"
                textAlign="start"
                fontSize="lg">Copyright By:</Text>
                <Input placeholder="Masukkan Copyright" size="md"
                input="text"
                marginBottom={4}
                defaultValue={data.copyright}
                ref={copyrightRef} />
            </Box>
        )
    }

    return (
        <>
            <Button colorScheme="green"
                size="lg"
                onClick={onOpen}>Atur Tampilan Awal</Button>
            <Modal onClose={onClose} size="4xl" isOpen={isOpen}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Atur Tampilan Awal</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        { setupDisplay() }
                    </ModalBody>
                    <ModalFooter>
                        <Button colorScheme="green"
                        size="lg"
                        isLoading={isDoUpdate}
                        onClick={onSubmit}>Simpan</Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}

export default Customize