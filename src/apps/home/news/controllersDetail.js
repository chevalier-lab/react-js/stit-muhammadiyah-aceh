import React from "react"
import { 
    Box, 
    Container,
    Flex
} from "@chakra-ui/react"
import { useParams } from "react-router"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import AllNews from "./views/allNews"
import WidgetNews from "./views/widgetNews"
import DetailNews from "./views/detailNews"

const NewsDetailController = () => {

    let { id } = useParams();

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <Box>
                        <Container maxW="container.xl"
                            bgColor="white"
                            paddingY={4}>
                            <Flex>
                                <Box flex={1}>
                                    <DetailNews id={id} />
                                    <AllNews />
                                </Box>
                                <Box width={{
                                    sm: "100%",
                                    md: "360px"
                                }}>
                                    <WidgetNews />
                                </Box>
                            </Flex>
                        </Container>
                    </Box>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default NewsDetailController