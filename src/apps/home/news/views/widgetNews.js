import React from "react"
import {
    Box,
    Tabs,
    TabList,
    Tab, 
    TabPanels,
    TabPanel,
    VStack,
    Flex,
    Text
} from "@chakra-ui/react"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import NewsAPI from "../../../../models/api/request/news"

const WidgetNewsItem = ({base_url, item}) => {

    let images = base_url + "/" + item.file_name
    images = images.replace("/public", "")
    const temp = images.split("\\")
    images = temp.join("/")

    return (
        <Flex paddingBottom={1}>
            <Box width="90px" height="64px"
                backgroundImage={`url('${images}')`}
                backgroundPosition="center"
                backgroundSize="cover"
                backgroundRepeat="no-repeat" />
            <VStack flex={1}
                marginStart={4}
                alignItems="start">
                <Text fontSize="md" fontWeight="semibold"
                    textColor="green">{(item.title.length > 32) ? item.title.substring(0, 32) : item.title}</Text>
                <Text textColor="gray">{item.username}</Text>
            </VStack>
        </Flex>
    )
}

const WidgetNewsBuilder = ({type, base_url, data}) => {

    return (
        <Box>
            {data.map((item, position) => {
                return <WidgetNewsItem item={item}
                    base_url={base_url}
                    key={"widget-news-" + type + "-" + position} />
            })}
        </Box>    
    )
}

const WidgetNews = () => {
    const [dataList, setDataList] = React.useState([])
    const [dataListPopular, setDataListPopular] = React.useState([])

    React.useEffect(() => {
        loadDataList("")
    }, [])

    const loadDataList = (search) => {
        NewsAPI.list(0, 10, search, item => {
            if (item.success) {
                setDataList(item.data)
            }
        })

        NewsAPI.popular(0, 10, search, item => {
            if (item.success) {
                setDataListPopular(item.data)
            }
        })
    }

    const setupDisplay = (provider) => {
        // const main_state = provider.main_state
        const base_url = ReqAPI.base_url_image
        // const news = main_state.sections.news
        const popular = dataListPopular
        const recent = dataList

        return (
            <Box>
                <Tabs>
                    <TabList>
                        <Tab>Populer</Tab>
                        <Tab>Terbaru</Tab>
                    </TabList>

                    <TabPanels>
                        <TabPanel>
                            <WidgetNewsBuilder
                                type="popular"
                                data={popular}
                                base_url={base_url} />
                        </TabPanel>
                        <TabPanel>
                            <WidgetNewsBuilder
                                type="recent"
                                data={recent}
                                base_url={base_url} />
                        </TabPanel>
                    </TabPanels>
                </Tabs>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default WidgetNews