import React from "react"
import {
    Box,
    VStack,
    Text,
    CircularProgress
} from "@chakra-ui/react"
import ReactMarkdown from "react-markdown"
import {
    WarningIcon
} from "@chakra-ui/icons"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import NewsAPI from "../../../../models/api/request/news"

const DetailNews = ({id}) => {
    const [data, setData] = React.useState(null)
    const [isLoadDetail, setIsLoadDetail] = React.useState(false)

    React.useEffect(() => {
        loadDataDetail()
    }, [])

    const loadDataDetail = () => {
        NewsAPI.detail(id, item => {
            if (item.success) {
                if (item.data.length > 0)
                    setData(item.data[0])
            }
            setIsLoadDetail(true)
        })
    }

    const setupDetail = (provider) => {
        // const main_state = provider.main_state
        const base_url = ReqAPI.base_url_image
        // const news = main_state.sections.news
        // const all_news = news.all_news.results
        const item = data

        if (!isLoadDetail) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat berita</Text>
            </Box>
        )

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf berita tidak ditemukan</Text>
            </Box>
        )

        let images = base_url + "/" + item.file_name
        images = images.replace("/public", "")
        const temp = images.split("\\")
        images = temp.join("/")

        return (
            <Box padding={2}>
                <Box width="100%"
                    height="512px"
                    backgroundImage={`url('${images}')`}
                    backgroundPosition="center"
                    backgroundSize="cover"
                    backgroundRepeat="no-repeat" />
                <VStack
                    marginTop={4}
                    alignItems="start">
                    <Text fontSize="xl" fontWeight="semibold"
                        textColor="green">{item.title}</Text>
                    <ReactMarkdown children={item.content} />
                    <Text fontSize="md">
                        <strong>Kategori: </strong>
                        {item.label}
                    </Text>
                    <Text fontSize="md">
                        <strong>Kata Kunci: </strong>
                        {item.tags}
                    </Text>
                    <Text fontSize="md">
                        <strong>Author: </strong>
                        {item.username}
                    </Text>
                </VStack>
                <br />
                <hr />
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDetail(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default DetailNews