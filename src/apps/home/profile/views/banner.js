import React from "react"
import { 
    Box,
    Container,
    CircularProgress,
    Text,
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import HomeAPI from "../../../../models/api/request/home"

const Banner = () => {
    const [isLoadData, setIsLoadData] = React.useState(false) 
    const [data, setData] = React.useState(null)

    React.useEffect(() => {
        HomeAPI.get(res => {
            if (res.success) {
                setData(res.data[0])
                setIsLoadData(true)
            }
        })
    }, [])

    const setupDisplay = (provider) => {
        const banner = data
        const base_url = ReqAPI.base_url_image

        if (!isLoadData) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat banner</Text>
            </Box>
        )

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf banner tidak ditemukan</Text>
            </Box>
        )

        let images = base_url + "/" + banner.file_name
        images = images.replace("/public", "")
        const temp = images.split("\\")
        images = temp.join("/")

        return (
            <Box>
                <Container maxW="container.xl"
                    bgColor="white"
                    paddingY={4}>
                    <Box
                        bgImage={`url('${images}')`}
                        backgroundPosition="center"
                        backgroundSize="cover"
                        height="512px"></Box>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default Banner