import React from "react"
import { 
    Box,
    Container,
    Text,
    CircularProgress,
    Flex
} from "@chakra-ui/react"
import {
    WarningIcon
} from "@chakra-ui/icons"
import ReactMarkdown from "react-markdown"

import MainContext from "../../../../utils/provider"
import HomeAPI from "../../../../models/api/request/home"

const About = () => {
    const [isLoadData, setIsLoadData] = React.useState(false) 
    const [data, setData] = React.useState(null)

    React.useEffect(() => {
        HomeAPI.get(res => {
            if (res.success) {
                setData(res.data[0])
                setIsLoadData(true)
            }
        })
    }, [])
    
    const setupDisplay = (provider) => {
        const about = data

        if (!isLoadData) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat data</Text>
            </Box>
        )

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf data tidak ditemukan</Text>
            </Box>
        )

        return (
            <Box>
                <Container maxW="container.xl"
                    bgColor="white"
                    paddingY={2}>
                    <Flex>
                        <Text fontSize="2xl"
                            fontWeight="semibold"
                            paddingBottom={2}
                            marginBottom={4}
                            textColor="green"
                            borderBottom="4px solid green">{about.title}</Text>
                        <Box flex={1} />
                    </Flex>
                    <ReactMarkdown children={about.content} />
                </Container>
            </Box>        
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default About