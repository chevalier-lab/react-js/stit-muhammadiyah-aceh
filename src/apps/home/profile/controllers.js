import React from "react"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import Banner from "./views/banner"
import About from "./views/about"

const ProfileController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <>
                        <Banner />
                        <About />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default ProfileController