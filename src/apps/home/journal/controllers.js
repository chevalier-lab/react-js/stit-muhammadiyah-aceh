import React from "react"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import AllJournal from "./views/allJournal"

const JournalController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <>
                        <AllJournal />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default JournalController