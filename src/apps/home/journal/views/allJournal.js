import React from "react"
import {
    Box,
    Container,
    Flex,
    VStack,
    Text,
    Button,
    IconButton,
    InputGroup,
    InputLeftElement,
    Input
} from "@chakra-ui/react"
import {
    ChevronLeftIcon,
    ChevronRightIcon,
    Search2Icon
} from "@chakra-ui/icons"
import { Link } from "react-router-dom"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import JournalsAPI from "../../../../models/api/request/journals"

const AllJournalItem = ({item}) => {

    return (
        <VStack padding={2}
            cursor="pointer"
            alignItems="start">
            <Text fontSize="xl" fontWeight="semibold"
                textColor="green">{(item.title.length > 32) ? item.title.substring(0, 32) : item.title}</Text>
            <Text dangerouslySetInnerHTML={{
                __html: (item.abstract.length > 480) ? item.abstract.substring(0, 480) + "..." : item.abstract
            }}/>
            <Button colorScheme="green"
                size="sm">Download Jurnal</Button>
        </VStack>
    )
}

const AllJournal = () => {
    const [dataList, setDataList] = React.useState([])
    const [page, setPage] = React.useState(0)
    const searchRef = React.useRef()

    React.useEffect(() => {
        loadDataList("")
    }, [])

    const loadDataList = (search) => {
        JournalsAPI.list(page, 10, search, item => {
            if (item.success) {
                setDataList(item.data)
                if (item.data.length === 0 && page > 0) setPage(page - 1)
            } else {
                if (page > 0) setPage(page - 1)
            }
        })
    }

    const setupDisplay = (provider) => {
        // const main_state = provider.main_state
        // const journal = main_state.sections.journal
        const all_journal = dataList
        
        return (
            <Box>
                <Container maxW="container.xl"
                    bgColor="white"
                    paddingY={4}>
                    <InputGroup>
                        <InputLeftElement
                            children={<Search2Icon />} />
                        <Input type="search" placeholder={"Cari jurnal berdasarkan judul, deskripsi atau lainnya..."}  variant="unstyled"
                        marginY={2}
                        ref={searchRef}
                        onChange={(e) => loadDataList(e.target.value)}/>
                    </InputGroup>
    
                    <Box marginBottom={4}>
                        <hr />
                    </Box>

                    {all_journal.map((item, position) => {
                        return <Link
                            to={"/journal/" + item.id}
                            key={"all-announcement-" + position}>
                            <AllJournalItem 
                            item={item} />
                        </Link>
                    })}

                    <Flex>
                        <IconButton icon={<ChevronLeftIcon />} colorScheme="gray" variant="outline"
                            margin={2}
                            onClick={() => {
                                if (page > 0)
                                    setPage(page - 1)
                                loadDataList(searchRef.current.value)
                            }} />
                        <IconButton icon={<ChevronRightIcon />} colorScheme="gray" variant="outline"
                            margin={2}
                            onClick={() => {
                                setPage(page + 1)
                                loadDataList(searchRef.current.value)
                            }} />
                    </Flex>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default AllJournal