import React from "react"
import {
    Box,
    Container,
    VStack,
    Text,
    Button,
} from "@chakra-ui/react"
import ReactMarkdown from "react-markdown"
import {
    WarningIcon
} from "@chakra-ui/icons"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import JournalsAPI from "../../../../models/api/request/journals"

const DetailJournal = ({id}) => {
    const [data, setData] = React.useState(null)

    React.useEffect(() => {
        loadDataDetail()
    }, [])

    const loadDataDetail = () => {
        JournalsAPI.detail(id, item => {
            if (item.success) {
                if (item.data.length > 0)
                    setData(item.data[0])
            }
        })
    }

    const setupDisplay = (provider) => {
        const item = data

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf jurnal tidak ditemukan</Text>
            </Box>
        )

        let images = ReqAPI.base_url_image + "/" + item.file_name
        images = images.replace("/public", "")
        const temp = images.split("\\")
        images = temp.join("/")
        const tempFilename = images.split("/")
        
        return (
            <Box>
                <Container maxW="container.xl"
                    bgColor="white"
                    paddingY={4}>

                    <Box padding={2}>
                        <VStack
                            alignItems="start">
                            <Text fontSize="xl" fontWeight="semibold"
                                textColor="green">{item.title}</Text>
                            <ReactMarkdown children={item.abstract} />
                            <Text fontSize="md">
                                <strong>Kata Kunci: </strong>
                                {item.keywords}
                            </Text>
                            <Text fontSize="md">
                                <strong>Author: </strong>
                                {item.authors}
                            </Text>
                            <Button colorScheme="green"
                                onClick={() => {
                                    const link = document.createElement('a');
                                    link.href = images;
                                    link.setAttribute(
                                        'download',
                                        tempFilename[tempFilename.length - 1],
                                    );
                                    link.click();
                                }}
                                size="sm">Download Jurnal</Button>
                        </VStack>
                        <br />
                        <hr />
                    </Box>
                    
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default DetailJournal