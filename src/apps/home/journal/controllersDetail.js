import React from "react"
import { useParams } from "react-router"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import AllJournal from "./views/allJournal"
import DetailJournal from "./views/detailJournal"

const JournalDetailController = () => {

    let { id } = useParams();

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <>
                        <DetailJournal id={id} />
                        <AllJournal />
                    </>
                )} />

            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default JournalDetailController