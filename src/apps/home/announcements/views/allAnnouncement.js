import React from "react"
import {
    Box,
    Container,
    Flex,
    VStack,
    Text,
    Button,
    IconButton,
    InputGroup,
    InputLeftElement,
    Input
} from "@chakra-ui/react"
import {
    ChevronLeftIcon,
    ChevronRightIcon,
    Search2Icon
} from "@chakra-ui/icons"
import { Link } from "react-router-dom"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import AnnouncementsAPI from "../../../../models/api/request/announcements"

const AllAnnouncementItem = ({base_url, item}) => {

    let images = base_url + "/" + item.file_name
    images = images.replace("/public", "")
    const temp = images.split("\\")
    images = temp.join("/")

    return (
        <Flex padding={2}
            cursor="pointer">
            <Box width="256px" height="180px"
                backgroundImage={`url('${images}')`}
                backgroundPosition="center"
                backgroundSize="cover"
                backgroundRepeat="no-repeat" />
            <VStack flex={1}
                marginStart={4}
                alignItems="start">
                <Text fontSize="xl" fontWeight="semibold"
                    textColor="green">{(item.title.length > 32) ? item.title.substring(0, 32) : item.title}</Text>
                <Text dangerouslySetInnerHTML={{
                    __html: (item.content.length > 480) ? item.content.substring(0, 480) + "..." : item.content
                }}/>
                <Button colorScheme="green"
                    size="sm">Baca Selengkapnya</Button>
            </VStack>
        </Flex>
    )
}

const AllAnnouncement = () => {
    const [dataList, setDataList] = React.useState([])
    const [page, setPage] = React.useState(0)
    const searchRef = React.useRef()

    React.useEffect(() => {
        loadDataList("")
    }, [])

    const loadDataList = (search) => {
        AnnouncementsAPI.list(page, 10, search, item => {
            if (item.success) {
                setDataList(item.data)
                if (item.data.length === 0 && page > 0) setPage(page - 1)
            } else {
                if (page > 0) setPage(page - 1)
            }
        })
    }

    const setupDisplay = (provider) => {
        // const main_state = provider.main_state
        const base_url = ReqAPI.base_url_image
        // const announcement = main_state.sections.announcement
        const all_announcement = dataList
        
        return (
            <Box>
                <Container maxW="container.xl"
                    bgColor="white"
                    paddingY={4}>
                    <InputGroup>
                        <InputLeftElement
                            children={<Search2Icon />} />
                        <Input type="search" placeholder={"Cari pengumuman berdasarkan judul, deskripsi atau lainnya..."}  variant="unstyled"
                        marginY={2}
                        ref={searchRef}
                        onChange={(e) => loadDataList(e.target.value)}/>
                    </InputGroup>
    
                    <Box marginBottom={4}>
                        <hr />
                    </Box>

                    {all_announcement.map((item, position) => {
                        return <Link to={"/announcement/" + item.id}
                            key={"all-announcement-" + position}>
                            <AllAnnouncementItem 
                            item={item}
                            base_url={base_url} />
                        </Link>
                    })}

                    <Flex>
                        <IconButton icon={<ChevronLeftIcon />} colorScheme="gray" variant="outline"
                            margin={2}
                            onClick={() => {
                                if (page > 0)
                                    setPage(page - 1)
                                loadDataList(searchRef.current.value)
                            }} />
                        <IconButton icon={<ChevronRightIcon />} colorScheme="gray" variant="outline"
                            margin={2}
                            onClick={() => {
                                setPage(page + 1)
                                loadDataList(searchRef.current.value)
                            }} />
                    </Flex>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default AllAnnouncement