import React from "react"
import {
    Box,
    Container,
    VStack,
    Text,
    CircularProgress
} from "@chakra-ui/react"
import ReactMarkdown from "react-markdown"
import {
    WarningIcon
} from "@chakra-ui/icons"

import MainContext from "../../../../utils/provider"
import { ReqAPI } from "../../../../models/api/req"
import AnnouncementsAPI from "../../../../models/api/request/announcements"

const DetailAnnouncement = ({id}) => {
    const [data, setData] = React.useState(null)
    const [isLoadDetail, setIsLoadDetail] = React.useState(false)

    React.useEffect(() => {
        loadDataDetail()
    }, [])

    const loadDataDetail = () => {
        AnnouncementsAPI.detail(id, item => {
            if (item.success) {
                if (item.data.length > 0)
                    setData(item.data[0])
            }
            setIsLoadDetail(true)
        })
    }

    const setupDisplay = (provider) => {
        // const main_state = provider.main_state
        const base_url = ReqAPI.base_url_image
        // const announcement = main_state.sections.announcement
        // const all_announcement = announcement.results
        const item = data

        if (!isLoadDetail) return (
            <Box alignContent="center"
                textAlign="center">
                <CircularProgress isIndeterminate color="green.300" />
                <Text>Sedang memuat pengumuman</Text>
            </Box>
        )

        if (data === null) return (
            <Box 
                alignContent="center"
                textAlign="center">
                <WarningIcon boxSize={16}
                    color="yellow" />
                <Text fontSize="2xl">404!!</Text>
                <Text>Maaf pengumuman tidak ditemukan</Text>
            </Box>
        )

        let images = base_url + "/" + item.file_name
        images = images.replace("/public", "")
        const temp = images.split("\\")
        images = temp.join("/")
        
        return (
            <Box>
                <Container maxW="container.xl"
                bgColor="white"
                paddingY={4}>
                    <Box padding={2}>
                        <Box width="100%"
                            height="512px"
                            backgroundImage={`url('${images}')`}
                            backgroundPosition="center"
                            backgroundSize="cover"
                            backgroundRepeat="no-repeat" />
                        <VStack
                            marginTop={4}
                            alignItems="start">
                            <Text fontSize="xl" fontWeight="semibold"
                                textColor="green">{item.title}</Text>
                            <ReactMarkdown children={item.content} />
                            <Text fontSize="md">
                                <strong>Kata Kunci: </strong>
                                {item.tags}
                            </Text>
                            <Text fontSize="md">
                                <strong>Author: </strong>
                                {item.username}
                            </Text>
                        </VStack>
                        <br />
                        <hr />
                    </Box>
                </Container>
            </Box>
        )
    }

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                { setupDisplay(provider) }
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default DetailAnnouncement