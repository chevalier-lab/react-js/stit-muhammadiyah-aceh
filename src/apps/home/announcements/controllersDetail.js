import React from "react"
import { useParams } from "react-router"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import AllAnnouncement from "./views/allAnnouncement"
import DetailAnnouncement from "./views/detailAnnouncement"

const AnnouncementsDetailController = () => {

    let { id } = useParams();

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <>
                        <DetailAnnouncement id={id} />  
                        <AllAnnouncement />  
                    </>
                )} />
                
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default AnnouncementsDetailController