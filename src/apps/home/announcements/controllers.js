import React from "react"

import MainContext from "../../../utils/provider"

import HomeUseCase from "../../../usecase/home"
import AllAnnouncement from "./views/allAnnouncement"

const AnnouncementsController = () => {

    return (
        <MainContext.Consumer>
            { provider => <React.Fragment>
                
                <HomeUseCase render={_ => (
                    <>
                        <AllAnnouncement />  
                    </>
                )} />
                
            </React.Fragment> }
        </MainContext.Consumer>
    )
}

export default AnnouncementsController