import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ChakraProvider } from "@chakra-ui/react";

import MainContext from "./utils/provider";
import Dataset from './models/dataset';
import SRCToScreens from './utils/screens';
import theme from "./utils/theme";

import AuthAPI from './models/api/request/auth';

class App extends Component {
    constructor(props) {
        super(props)
        const temp = Dataset
        let auth = window.localStorage.getItem(temp.db)
        if (auth !== null) {
            auth = JSON.parse(auth)
            temp.is_auth = 1
            temp.auth = auth.active_key

            // Check Auth
            this.onCheckAuth(temp.db, auth)
        } 

        this.state = temp
    }

    // OnCheckAuth
    onCheckAuth = (db, auth) => {
        AuthAPI.checkSession(auth.active_key, res => {
            if (!res.success) {
                auth.active_key = null
                AuthAPI.setSession(auth.id, auth, _ => {
                    window.localStorage.removeItem(db)
                    window.alert(res.message)
                    window.location.reload("/")
                })
            }
        })
    }

    // Update State
    onUpdateState = (newState) => this.setState(newState)

    // Injector Display
    injectorDisplay = () => {
        const routes = []

        // Inject Main Routes
        this.state.menus.main.forEach(item => routes.push(item))

        // Inject Top Routes
        this.state.menus.top.forEach(item => {
            if (this.state.is_auth !== 0 && item.type === "auth") routes.push(item) 
            else if (item.type === "non-auth") routes.push(item)
        })

        // Inject Side Routes
        this.state.menus.sidebar.forEach(item => {
            if (this.state.is_auth !== 0 && item.aria !== "dashboard") 
            routes.push(item)
        })

        return (
            <Switch>
                {routes.map((item) => {
                    return <Route exact path={item.src} component={SRCToScreens[item.aria]} 
                    key={"routing-" + item.id}/>
                })}
            </Switch>
        )
    }

    render = () => (
        <ChakraProvider theme={theme}>
            <MainContext.Provider value={{
                main_state: this.state,
                update_state: this.onUpdateState
            }}>
                <React.Fragment>
                    <Router>
                        {this.injectorDisplay()}
                    </Router>
                </React.Fragment>
            </MainContext.Provider>
        </ChakraProvider>
    )
}

export default App