import ProfileController from "../apps/home/profile/controllers"
import NewsController from "../apps/home/news/controllers"
import AnnouncementsController from "../apps/home/announcements/controllers"
import JournalController from "../apps/home/journal/controllers"
import ManageProfileController from "../apps/dashboard/profile/controllers"
import ManageAccountsController from "../apps/dashboard/accounts/controllers"
import ManageNewsController from "../apps/dashboard/news/controllers"
import ManageAnnouncementsController from "../apps/dashboard/announcements/controllers"
import ManageJournalController from "../apps/dashboard/journal/controllers"
import NewsDetailController from "../apps/home/news/controllersDetail"
import AnnouncementsDetailController from "../apps/home/announcements/controllersDetail"
import JournalDetailController from "../apps/home/journal/controllersDetail"
import LoginController from "../apps/portal/login/controllers"

const SRCToScreens = {
    // Main
    profile: ProfileController,
    news: NewsController,
    news_detail: NewsDetailController,
    announcement: AnnouncementsController,
    announcement_detail: AnnouncementsDetailController,
    journal: JournalController,
    journal_detail: JournalDetailController,
    // Portal
    login: LoginController,
    // Top
    dashboard: ManageProfileController,
    account: ManageAccountsController,
    // Sidebar
    manage_news: ManageNewsController,
    manage_announcements: ManageAnnouncementsController,
    manage_journal: ManageJournalController
}

export default SRCToScreens