const Menus = {
    sidebar: [
        {
            id: "sidebar-menu-dashboard",
            aria: "dashboard",
            src: "/dashboard",
            type: "auth",
            label: "Dashboard"
        },
        {
            id: "sidebar-menu-manage-news",
            aria: "manage_news",
            src: "/manage-news",
            type: "auth",
            label: "Kelola Berita"
        },
        {
            id: "sidebar-menu-manage-announcements",
            aria: "manage_announcements",
            src: "/manage-announcements",
            type: "auth",
            label: "Kelola Pengumuman"
        },
        {
            id: "sidebar-menu-manage-journal",
            aria: "manage_journal",
            src: "/manage-journal",
            type: "auth",
            label: "Kelola Jurnal"
        }
    ],
    top: [
        {
            id: "top-menu-home",
            aria: "profile",
            ariaChildren: [
                "/",
                "/news",
                "/news/:id",
                "/announcement",
                "/announcement/:id",
                "/journal",
                "/journal/:id",
            ],
            src: "/",
            type: "general",
            label: "Home"
        },
        {
            id: "top-menu-dashboard",
            aria: "dashboard",
            ariaChildren: [],
            src: "/dashboard",
            type: "auth",
            label: "Dashboard"
        },
        {
            id: "top-menu-login",
            aria: "login",
            ariaChildren: [],
            src: "/login",
            type: "non-auth",
            label: "Masuk"
        }
    ],
    main: [
        {
            id: "main-menu-profile",
            aria: "profile",
            src: "/",
            type: "menu",
            label: "Profile"
        },
        {
            id: "main-menu-berita",
            aria: "news",
            src: "/news",
            type: "menu",
            label: "Berita"
        },
        {
            id: "main-menu-berita-detail",
            aria: "news_detail",
            src: "/news/:id",
            type: "detail",
            label: "Berita Detail"
        },
        {
            id: "main-menu-pengumuman",
            aria: "announcement",
            src: "/announcement",
            type: "menu",
            label: "Pengumuman"
        },
        {
            id: "main-menu-pengumuman-detail",
            aria: "announcement_detail",
            src: "/announcement/:id",
            type: "detail",
            label: "Pengumuman Detail"
        },
        {
            id: "main-menu-journal",
            aria: "journal",
            src: "/journal",
            type: "menu",
            label: "Jurnal Anatesa"
        },
        {
            id: "main-menu-journal-detail",
            aria: "journal_detail",
            src: "/journal/:id",
            type: "detail",
            label: "Jurnal Anatesa Detail"
        },
    ]
}

const Images = {
    logo: {
        main: "logo/logo.png",
        founder: "logo/founder.png",
    },
    banner: "banner/1.jpg",
    bg: {
        motif: "bg/motif.png"
    },
    news: "news/dummy.png",
    announcement: "announcement/dummy.png",
}

const Header = {
    logo: Images.logo,
    title: "SEKOLAH TINGGI ILMU TARBIYAH <br />MUHAMMADIYAH ACEH BARAT DAYA",
    tagline: "Unggul, Professional, Berkarakter Islam",
    author: "Mukhlis MS, MA<br />Ketua"
}

const Footer = {
    motif: Images.bg.motif,
    address: "Jl. Nasional (Komplek Pendidikan) Padang Meurante Aceh Barat Daya, Kab/Kota Aceh Barat, Prov Aceh, Indonesia",
    phone: "+6282119189690",
    copyright: "Copyright by stitmuhammadiyaacehbaratdaya.ac.id"
}

const News = [
    {
        thumbnail: Images.news,
        title: "Berita Satu Tentang Berita",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    },
    {
        thumbnail: Images.news,
        title: "Berita Dua Tentang Berita",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    }
]

const Announcement = [
    {
        thumbnail: Images.announcement,
        title: "Pengumuman 1",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    },
    {
        thumbnail: Images.announcement,
        title: "Pengumuman 1",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    }
]

const Journal = [
    {
        title: "Judul Jurnal 1",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    },
    {
        title: "Judul Jurnal 1",
        author: "Administrator",
        contents: [
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]
    }
]

const Sections = {
    login: {
        fields: {
            username: {
                label: "Username",
                hint: "Masukkan Username"
            },
            password: {
                label: "Password",
                hint: "Masukkan Password"
            },
            submit: {
                label: "Masuk"
            }
        }
    },
    home: {
        profile: {
            banner: Images.banner,
            about: {
                title: "Sejarah STIT Muhammadiyah Aceh Barat Daya",
                contents: [
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                ]
            }
        }
    },
    news: {
        all_news: {
            page: 0,
            limit: 12,
            results: News
        },
        widget_news: {
            popular: News,
            recent: News
        }
    },
    announcement: {
        page: 0,
        limit: 12,
        results: Announcement
    },
    journal: {
        page: 0,
        limit: 12,
        results: Journal
    }
}

const Dataset = {
    base_url: "http://213.190.4.40/storages/muhammadiyah/",
    menus: Menus,
    header: Header,
    footer: Footer,
    images: Images,
    sections: Sections,
    is_auth: 0,
    db: "stit-muhammadiyah-aceh",
    auth: ""
}

export default Dataset