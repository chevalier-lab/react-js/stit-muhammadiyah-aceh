// Request Filter
const ReqFilter = {}
ReqFilter.toURI = (url = "", q={}) => {
    let newURL = url + "?" + Object.keys(q)
        .map(k => k + '=' + encodeURIComponent(q[k])).join('&')
    return newURL
}
ReqFilter.toJSON = (response) => response.json()

// Request Method
const ReqMethod = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    PATCH: "PATCH",
    DELETE: "DELETE",
}

// Request API Core
const ReqAPI = {}

// Request API Base URL
// ReqAPI.base_url = "http://213.190.4.40:2024/api"
// ReqAPI.base_url_image = "http://213.190.4.40:2024"
ReqAPI.base_url = "http://localhost:2024/api"
ReqAPI.base_url_image = "http://localhost:2024"

// Request API Auth
ReqAPI.auth = {}
ReqAPI.auth.base = ReqAPI.base_url + "/auth"
ReqAPI.auth.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.auth.base, {
        page: page,
        limit: limit
    })
ReqAPI.auth.id = (id=-1) => ReqAPI.auth.base + "/" + id
ReqAPI.auth.active_key = (active_key="") => ReqAPI.auth.base + "/" + active_key

// Request API User
ReqAPI.users = {}
ReqAPI.users.base = ReqAPI.base_url + "/users"
ReqAPI.users.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.users.base, {
        page: page,
        limit: limit
    })
ReqAPI.users.id = (id=-1) => ReqAPI.users.base + "/" + id

// Request API News
ReqAPI.news = {}
ReqAPI.news.base = ReqAPI.base_url + "/news"
ReqAPI.news.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.news.base, {
        page: page,
        limit: limit
    })
ReqAPI.news.id = (id=-1) => ReqAPI.news.base + "/" + id

// Request API Announcement
ReqAPI.announcements = {}
ReqAPI.announcements.base = ReqAPI.base_url + "/announcements"
ReqAPI.announcements.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.announcements.base, {
        page: page,
        limit: limit
    })
ReqAPI.announcements.id = (id=-1) => ReqAPI.announcements.base + "/" + id

// Request API Journal
ReqAPI.journals = {}
ReqAPI.journals.base = ReqAPI.base_url + "/journals"
ReqAPI.journals.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.journals.base, {
        page: page,
        limit: limit
    })
ReqAPI.journals.id = (id=-1) => ReqAPI.journals.base + "/" + id

// Request API Categories
ReqAPI.categories = {}
ReqAPI.categories.base = ReqAPI.base_url + "/categories"
ReqAPI.categories.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.categories.base, {
        page: page,
        limit: limit
    })
ReqAPI.categories.id = (id=-1) => ReqAPI.categories.base + "/" + id

// Request API Logs
ReqAPI.logs = {}
ReqAPI.logs.base = ReqAPI.base_url + "/logs"
ReqAPI.logs.pagination = (page=1, limit=10) => 
    ReqFilter.toURI(ReqAPI.logs.base, {
        page: page,
        limit: limit
    })
ReqAPI.logs.id = (id=-1) => ReqAPI.logs.base + "/" + id

// Request API Home
ReqAPI.home = {}
ReqAPI.home.base = ReqAPI.base_url + "/home"
ReqAPI.home.banner = ReqAPI.home.base + "/banner"

export {
    ReqAPI,
    ReqMethod,
    ReqFilter
}