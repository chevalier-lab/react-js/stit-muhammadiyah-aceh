import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const AnnouncementsAPI = {}
AnnouncementsAPI.create = (api_key="", data, callback) => fetch(
    ReqAPI.announcements.base, {
        method: ReqMethod.POST,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AnnouncementsAPI.count = (callback) => fetch(
    ReqAPI.announcements.base + "/count", {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AnnouncementsAPI.list = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.announcements.base, {
        q: q,
        limit: limit,
        page: page
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AnnouncementsAPI.delete = (id=-1, callback) => fetch(
    ReqAPI.announcements.id(id), {
        method: ReqMethod.DELETE,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AnnouncementsAPI.detail = (id=-1, callback) => fetch(
    ReqAPI.announcements.id(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AnnouncementsAPI.update = (api_key="", id, data, callback) => fetch(
    ReqAPI.announcements.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default AnnouncementsAPI