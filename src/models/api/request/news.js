import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const NewsAPI = {}
NewsAPI.create = (api_key="", data, callback) => fetch(
    ReqAPI.news.base, {
        method: ReqMethod.POST,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.count = (callback) => fetch(
    ReqAPI.news.base + "/count", {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.list = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.news.base, {
        q: q,
        limit: limit,
        page: page
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.popular = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.news.base + "/popular", {
        q: q,
        limit: limit,
        page: page
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.delete = (id=-1, callback) => fetch(
    ReqAPI.news.id(id), {
        method: ReqMethod.DELETE,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.detail = (id=-1, callback) => fetch(
    ReqAPI.news.id(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

NewsAPI.update = (api_key="", id, data, callback) => fetch(
    ReqAPI.news.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default NewsAPI