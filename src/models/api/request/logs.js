import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const LogsAPI = {}

LogsAPI.list = (page=0, limit=10, callback) => fetch(
    ReqFilter.toURI(ReqAPI.logs.base, {
        page: page,
        limit: limit
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default LogsAPI