import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const UsersAPI = {}
UsersAPI.list = (callback) => fetch(
    ReqAPI.users.base, {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.count = (callback) => fetch(
    ReqAPI.users.base + "/count", {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.filter = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.users.base, {
        page: page,
        limit: limit,
        q: q
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.create = (api_key="", newData={}, callback) => fetch(
    ReqAPI.users.base, {
        method: ReqMethod.POST,
        headers: {
            "x-api-key": api_key,
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newData)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.delete = (id=-1, callback) => fetch(
    ReqAPI.users.id(id), {
        method: ReqMethod.DELETE,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

UsersAPI.update = (api_key="", id, newData={}, callback) => fetch(
    ReqAPI.users.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "x-api-key": api_key,
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newData)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default UsersAPI