import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const HomeAPI = {}
HomeAPI.get = (callback) => fetch(
    ReqAPI.home.base, {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

HomeAPI.update = (data, callback) => fetch(
    ReqAPI.home.base, {
        method: ReqMethod.PUT,
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

HomeAPI.banner = (data, callback) => fetch(
    ReqAPI.home.banner, {
        method: ReqMethod.PUT,
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default HomeAPI