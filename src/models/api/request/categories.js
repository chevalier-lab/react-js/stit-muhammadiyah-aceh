import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const CategoriesAPI = {}
CategoriesAPI.list = (callback) => fetch(
    ReqAPI.categories.base, {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

CategoriesAPI.filter = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.categories.base + "/filter", {
        page: page,
        limit: limit,
        q: q
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

CategoriesAPI.create = (api_key="", newData={}, callback) => fetch(
    ReqAPI.categories.base, {
        method: ReqMethod.POST,
        headers: {
            "x-api-key": api_key,
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newData)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

CategoriesAPI.delete = (id=-1, callback) => fetch(
    ReqAPI.categories.id(id), {
        method: ReqMethod.DELETE,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

CategoriesAPI.update = (api_key="", id, newData={}, callback) => fetch(
    ReqAPI.categories.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "x-api-key": api_key,
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newData)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default CategoriesAPI