import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const JournalsAPI = {}
JournalsAPI.create = (api_key="", data, callback) => fetch(
    ReqAPI.journals.base, {
        method: ReqMethod.POST,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JournalsAPI.count = (callback) => fetch(
    ReqAPI.journals.base + "/count", {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JournalsAPI.detail = (id=-1, callback) => fetch(
    ReqAPI.journals.id(id), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JournalsAPI.list = (page=0, limit=10, q="", callback) => fetch(
    ReqFilter.toURI(ReqAPI.journals.base, {
        q: q,
        limit: limit,
        page: page
    }), {
        method: ReqMethod.GET,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

JournalsAPI.delete = (id=-1, callback) => fetch(
    ReqAPI.journals.id(id), {
        method: ReqMethod.DELETE,
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))


JournalsAPI.update = (api_key="", id, data, callback) => fetch(
    ReqAPI.journals.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "x-api-key": api_key
        },
        body: data
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

export default JournalsAPI