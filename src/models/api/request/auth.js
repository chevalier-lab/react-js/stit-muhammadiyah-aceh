import { ReqAPI, ReqMethod, ReqFilter } from "../req"

const AuthAPI = {}
AuthAPI.login = (uname="", passw="", callback) => fetch(
    ReqAPI.auth.base, {
        method: ReqMethod.POST,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify({
            username: uname,
            password: passw
        })
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AuthAPI.setSession = (id=-1, newData={}, callback) => fetch(ReqAPI.users.id(id), {
        method: ReqMethod.PUT,
        headers: {
            "Content-Type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(newData)
    }).then(ReqFilter.toJSON)
    .then(item => callback(item))
    .catch(error => callback(error))

AuthAPI.checkSession = (active_key="", callback) => fetch(ReqAPI.auth.active_key(active_key), {
    method: ReqMethod.GET,
}).then(ReqFilter.toJSON)
.then(item => callback(item))
.catch(error => callback(error))

export default AuthAPI