import React from "react"
import { 
    Box, Flex
} from "@chakra-ui/react"

import MainContext from "../utils/provider"
import Sidebar from "../components/sidebar"
import TopBar from "../components/topbar"

class DashboardUseCase extends React.Component {

    setupUseCase = (provider) => {
        const pathname = window.location.pathname
        const main_state = provider.main_state
        const top_menu = main_state.menus.top.filter(item => {
            if (main_state.is_auth !== 0 && item.type === "auth") return true 
            else if (item.type === "non-auth") return false
            else if (item.type === "general") return true
            return false
        })

        return (
            <Flex
                bgColor="gray.50"
                height="100vh"
                width="100%"
                position="relative"
                overflowX="hidden">

                <Box
                    width="360px"
                    bgColor="#233819"
                    color="white"
                    borderRight="1px solid rgba(0,0,0,0.1)">
                    <Sidebar provider={provider} />
                </Box>

                <Box flex={1}>
                    <TopBar 
                        menus={top_menu}
                        path={pathname} />
                    {this.props.render(this.state)}
                </Box>

            </Flex>
        )
    }
    
    render = () => {
        return (
            <MainContext.Consumer>
                { provider => <React.Fragment>
                    { this.setupUseCase(provider) }
                </React.Fragment> }
            </MainContext.Consumer>
        )
    }
}

export default DashboardUseCase