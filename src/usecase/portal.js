import React from "react"
import { Box } from "@chakra-ui/react"

import MainContext from "../utils/provider"

class PortalUseCase extends React.Component {

    setupUseCase = (provider) => {
        // const main_state = provider.main_state

        return (
            <Box
                bgColor="gray.50"
                height="100vh">
                {this.props.render(this.state)}
            </Box>
        )
    }
    
    render = () => {
        return (
            <MainContext.Consumer>
                { provider => <React.Fragment>
                    { this.setupUseCase(provider) }
                </React.Fragment> }
            </MainContext.Consumer>
        )
    }
}

export default PortalUseCase