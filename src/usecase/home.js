import React from "react"
import { Box } from "@chakra-ui/react"

import MainContext from "../utils/provider"

import TopBar from "../components/topbar"
import Header from "../components/header"
import Mainbar from "../components/mainbar"
import Footer from "../components/footer"

class HomeUseCase extends React.Component {

    setupUseCase = (provider) => {
        const pathname = window.location.pathname

        const main_state = provider.main_state
        const main_menu = main_state.menus.main.filter(item => {
            return (item.type === "menu")
        })
        const header = main_state.header
        const footer = main_state.footer
        let top_menu = []
        if (main_state.is_auth) 
        top_menu = main_state.menus.top.filter(item => {
            if (main_state.is_auth !== 0 && item.type === "auth") return true 
            else if (item.type === "non-auth") return false
            else if (item.type === "general") return true
            return false
        })
        else top_menu = main_state.menus.top.filter(item => {
            if (item.type === "non-auth") return true
            else if (item.type === "general") return true
            return false
        })

        return (
            <Box
                bgColor="gray.50">
                <TopBar 
                    menus={top_menu}
                    path={pathname} />
                <Header data={header} />
                <Mainbar menus={main_menu}
                    path={pathname} />
                {this.props.render(this.state)}
                <Footer footer={footer} />
            </Box>
        )
    }
    
    render = () => {
        return (
            <MainContext.Consumer>
                { provider => <React.Fragment>
                    { this.setupUseCase(provider) }
                </React.Fragment> }
            </MainContext.Consumer>
        )
    }
}

export default HomeUseCase