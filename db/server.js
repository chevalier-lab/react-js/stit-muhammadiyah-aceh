const express = require('express');
const bodyParser = require('body-parser');
const koneksi = require('./config/database');

const multer = require('multer');
var path = require('path');     //used for file path
var fs = require('fs-extra');
const helpers = require('./config/helpers');

const cors = require('cors')

const app = express();
const PORT = 2024;

var whitelist = ['http://213.190.4.40:2023', 'http://localhost:3000', 'http://localhost:2024', '*']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true)
    } else {
      callback(null, false)
    }
  }
}

app.use('*', cors(corsOptions))

// set body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const directory = express.static(path.join(__dirname, '/public'))
app.use(directory);
app.use("/uploads", directory);


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/uploads/');
    },

    // By default, multer removes file extensions so let's add them back
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
});

const upload = multer({ storage: storage})

const newLogs = (req, activity) => {
    const querySql = 'INSERT INTO logs SET ?';

    // jalankan query
    koneksi.query(querySql, {
        activity: activity,
        ip_address: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }, (err, rows, field) => {});
}

// read data / get data
app.post('/api/auth', (req, res) => {
    // buat query sql
    const data = { ...req.body };
    const querySql = 'SELECT id, active_key FROM users WHERE username = ? AND password = ?';

    // jalankan query
    koneksi.query(querySql, [data.username, data.password], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        newLogs(req, "Berhasil Login")
        res.status(200).json({ success: true, data: rows });
    });
});

// read data / get data
app.get('/api/auth/:active_key', (req, res) => {
    // buat query sql
    const querySql = 'SELECT id FROM users WHERE active_key = ?';

    // jalankan query
    koneksi.query(querySql, req.params.active_key, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.get('/api/users/count', (req, res) => {
    // buat query sql
    const querySql = 'SELECT COUNT(id) as total FROM users';

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// create data / insert data
app.post('/api/users', (req, res) => {
    // buat variabel penampung data dan query sql
    const data = { ...req.body };
    const querySql = 'INSERT INTO users SET ?';

    // jalankan query
    koneksi.query(querySql, data, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }

        // jika request berhasil
        newLogs(req, "Berhasil menambahkan data pengguna")
        res.status(201).json({ success: true, message: 'Berhasil insert data!' });
    });
});

// update data
app.put('/api/users/:id', (req, res) => {
    // buat variabel penampung data dan query sql
    const data = { ...req.body };
    const querySearch = 'SELECT * FROM users WHERE id = ?';
    const queryUpdate = 'UPDATE users SET ? WHERE id = ?';

    // jalankan query untuk melakukan pencarian data
    koneksi.query(querySearch, req.params.id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            // jalankan query update
            koneksi.query(queryUpdate, [data, req.params.id], (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                }

                // jika update berhasil
                newLogs(req, "Berhasil mengubah data pengguna")
                res.status(200).json({ success: true, message: 'Berhasil update data!' });
            });
        } else {
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
        }
    });
});

// read data / get data
app.get('/api/users', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    const q = req.query.q
    // buat query sql
    const querySql = 'SELECT * FROM users WHERE username LIKE ? ORDER BY created_at DESC LIMIT ? OFFSET ?';

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.delete('/api/users/:id', (req, res) => {
    // buat query sql
    const querySql = 'DELETE FROM users WHERE id = ?';
    const id = req.params.id;

    // jalankan query
    koneksi.query(querySql, id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.affectedRows > 0) {
            newLogs(req, "Berhasil menghapus data pengguna")
            res.status(200).json({ success: true, message: 'Berhasil menghapus data!' });
        }
        else 
            return res.status(404).json({ success: false, message: 'Gagal menghapus data!' });
    });
});

// read data / get data
app.get('/api/categories', (req, res) => {
    // buat query sql
    const querySql = 'SELECT * FROM m_categories ORDER BY label DESC';

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.get('/api/categories/filter', (req, res) => {
    const q = req.query.q
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    // buat query sql
    const querySql = 'SELECT * FROM m_categories WHERE label LIKE ? ORDER BY label DESC LIMIT ? OFFSET ?';

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        limit,
        offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.post('/api/categories', (req, res) => {
    // buat query sql
    const querySql = 'INSERT INTO m_categories SET ?';
    const data = { ...req.body };

    // jalankan query
    koneksi.query(querySql, data, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.affectedRows > 0) {
            newLogs(req, "Berhasil menambahkan data kategori")
            res.status(200).json({ success: true, message: 'Berhasil membuat data!' });
        }
        else 
            return res.status(404).json({ success: false, message: 'Gagal membuat data!' });
    });
});

// read data / get data
app.delete('/api/categories/:id', (req, res) => {
    // buat query sql
    const querySql = 'DELETE FROM m_categories WHERE id = ?';
    const id = req.params.id;

    // jalankan query
    koneksi.query(querySql, id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.affectedRows > 0) {
            newLogs(req, "Berhasil menghapus data kategori")
            res.status(200).json({ success: true, message: 'Berhasil menghapus data!' });
        }
        else 
            return res.status(404).json({ success: false, message: 'Gagal menghapus data!' });
    });
});

// read data / get data
app.put('/api/categories/:id', (req, res) => {
    // buat query sql
    const querySql = 'UPDATE m_categories SET ? WHERE id = ?';
    const id = req.params.id;
    const body = { ...req.body }

    // jalankan query
    koneksi.query(querySql, [
        body,
        id
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.affectedRows > 0) {
            newLogs(req, "Berhasil mengubah data kategori")
            res.status(200).json({ success: true, message: 'Berhasil mengubah data!' });
        }
        else 
            return res.status(404).json({ success: false, message: 'Gagal mengubah data!' });
    });
});

// read data / get data
app.post('/api/news',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlMedias = 'INSERT INTO m_medias SET ?';
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'INSERT INTO news SET ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (req.files.length === 0) {
        return res.status(500).json({ success: false, message: 'Harap pilih cover terlebih dahulu' });
    }

    // Display uploaded image for user validation
    const reqFile = req.files[0]
    koneksi.query(querySqlMedias, {
        file_name: reqFile.path,
        location: reqFile.path
    }, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }

        if (rows.affectedRows > 0) {

            const coverId = rows.insertId
            
            koneksi.query(querySqlAuthor, header, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
                console.log(rows[0].id)
                if (rows.length > 0) {
                    const authorId = rows[0].id

                    koneksi.query(querySql, {
                        title: body.title,
                        content: body.content,
                        tags: body.tags,
                        categoryId: body.categoryId,
                        coverId: coverId,
                        authorId: authorId
                    }, (err, rows, field) => {
                        // error handling
                        if (err) {
                            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                        }
                
                        // jika request berhasil
                        newLogs(req, "Berhasil menambahkan data berita")
                        res.status(201).json({ success: true, message: 'Berhasil insert data!' });
                    });
                } else {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
            });

        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!' });
        }

    });
});

// read data / get data
app.put('/api/news/:id',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'UPDATE news SET ? WHERE id = ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    const id = req.params.id
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    koneksi.query(querySqlAuthor, header, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
        console.log(rows[0].id)
        if (rows.length > 0) {
            const authorId = rows[0].id

            koneksi.query(querySql, [
                {
                    title: body.title,
                    content: body.content,
                    tags: body.tags,
                    categoryId: body.categoryId,
                },
                id
            ], (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
        
                // jika request berhasil
                newLogs(req, "Berhasil mengubah data berita")
                res.status(201).json({ success: true, message: 'Berhasil update data!' });
            });
        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
    });
});

app.get('/api/news', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    const q = req.query.q

    console.log(page, " | ", limit, " | ", offset)

    const querySql = "SELECT news.*, m_categories.label, m_medias.file_name, m_medias.location, users.username FROM news JOIN m_categories ON m_categories.id = news.categoryId JOIN m_medias ON m_medias.id = news.coverId JOIN users ON users.id = news.authorId WHERE news.title LIKE ? OR news.content LIKE ? ORDER BY news.id DESC LIMIT ? OFFSET ?";

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        '%'+q+'%',
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

app.get('/api/news/popular', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    const q = req.query.q

    console.log(page, " | ", limit, " | ", offset)

    const querySql = "SELECT news.*, m_categories.label, m_medias.file_name, m_medias.location, users.username FROM news JOIN m_categories ON m_categories.id = news.categoryId JOIN m_medias ON m_medias.id = news.coverId JOIN users ON users.id = news.authorId JOIN news_view ON news_view.newsId = news.id WHERE news.title LIKE ? OR news.content LIKE ? GROUP BY news.id ORDER BY news.id DESC LIMIT ? OFFSET ?";

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        '%'+q+'%',
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

const countViewNews = (req, id) => {
    const querySql = "INSERT INTO news_view SET ?"

    koneksi.query(querySql, {
        newsId: id,
        ip_address: req.headers['x-forwarded-for'] || req.connection.remoteAddress
    }, (err, rows, field) => {});
}

// read data / get data
app.get('/api/news/count', (req, res) => {
    // buat query sql
    const querySql = 'SELECT COUNT(id) as total FROM news';

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!', data: rows });
    });
});

app.get('/api/news/:id', (req, res) => {
    // buat query sql
    const id = req.params.id

    const querySql = "SELECT news.*, m_categories.label, m_medias.file_name, m_medias.location, users.username FROM news JOIN m_categories ON m_categories.id = news.categoryId JOIN m_medias ON m_medias.id = news.coverId JOIN users ON users.id = news.authorId WHERE news.id = ?";

    // jalankan query
    koneksi.query(querySql, id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length) {
            countViewNews(req, id)
            res.status(200).json({ success: true, data: rows });
        }
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

// update data
app.delete('/api/news/:id', (req, res) => {
    // buat variabel penampung data dan query sql
    const querySearch = 'SELECT coverId FROM news WHERE id = ?';
    const queryDeleteCover = 'DELETE FROM m_medias WHERE id = ?';
    const queryDeleteNews = 'DELETE FROM news WHERE id = ?';

    const id = req.params.id

    // jalankan query untuk melakukan pencarian data
    koneksi.query(querySearch, req.params.id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            // jalankan query update
            const coverId = rows[0].coverId
            koneksi.query(queryDeleteCover, coverId, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                }
        
                // jika id yang dimasukkan sesuai dengan data yang ada di db
                koneksi.query(queryDeleteNews, id, (err, rows, field) => {
                    // error handling
                    if (err) {
                        return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                    }
            
                    // jika id yang dimasukkan sesuai dengan data yang ada di db
                    newLogs(req, "Berhasil menghapus data berita")
                    return res.status(200).json({ success: true, message: 'Data berhasil dihapus' });
                });
            });
        } else {
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
        }
    });
});

// read data / get data
app.get('/api/announcements/count', (req, res) => {
    // buat query sql
    const querySql = 'SELECT COUNT(id) as total FROM announcements';

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.post('/api/announcements',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlMedias = 'INSERT INTO m_medias SET ?';
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'INSERT INTO announcements SET ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (req.files.length === 0) {
        return res.status(500).json({ success: false, message: 'Harap pilih cover terlebih dahulu' });
    }

    // Display uploaded image for user validation
    const reqFile = req.files[0]
    koneksi.query(querySqlMedias, {
        file_name: reqFile.path,
        location: reqFile.path
    }, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }

        if (rows.affectedRows > 0) {

            const coverId = rows.insertId
            
            koneksi.query(querySqlAuthor, header, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
                console.log(rows[0].id)
                if (rows.length > 0) {
                    const authorId = rows[0].id

                    koneksi.query(querySql, {
                        title: body.title,
                        content: body.content,
                        tags: body.tags,
                        coverId: coverId,
                        authorId: authorId
                    }, (err, rows, field) => {
                        // error handling
                        if (err) {
                            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                        }
                
                        // jika request berhasil
                        newLogs(req, "Berhasil menambahkan data pengumuman")
                        res.status(201).json({ success: true, message: 'Berhasil insert data!' });
                    });
                } else {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
            });

        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!' });
        }

    });
});

// read data / get data
app.put('/api/announcements/:id',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'UPDATE announcements SET ? WHERE id = ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    const id = req.params.id
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    koneksi.query(querySqlAuthor, header, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
        console.log(rows[0].id)
        if (rows.length > 0) {
            const authorId = rows[0].id

            koneksi.query(querySql, [
                {
                    title: body.title,
                    content: body.content,
                    tags: body.tags,
                },
                id
            ], (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
        
                // jika request berhasil
                newLogs(req, "Berhasil mengubah data pengumuman")
                res.status(201).json({ success: true, message: 'Berhasil update data!' });
            });
        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
    });
});

app.get('/api/announcements', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    const q = req.query.q

    console.log(page, " | ", limit, " | ", offset)

    const querySql = "SELECT announcements.*, m_medias.file_name, m_medias.location, users.username FROM announcements JOIN m_medias ON m_medias.id = announcements.coverId JOIN users ON users.id = announcements.authorId WHERE announcements.title LIKE ? OR announcements.content LIKE ? ORDER BY announcements.id DESC LIMIT ? OFFSET ?";

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        '%'+q+'%',
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

app.get('/api/announcements/:id', (req, res) => {
    // buat query sql
    const id = req.params.id

    const querySql = "SELECT announcements.*, m_medias.file_name, m_medias.location, users.username FROM announcements JOIN m_medias ON m_medias.id = announcements.coverId JOIN users ON users.id = announcements.authorId WHERE announcements.id = ?";

    // jalankan query
    koneksi.query(querySql, id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

// update data
app.delete('/api/announcements/:id', (req, res) => {
    // buat variabel penampung data dan query sql
    const querySearch = 'SELECT coverId FROM announcements WHERE id = ?';
    const queryDeleteCover = 'DELETE FROM m_medias WHERE id = ?';
    const queryDeleteNews = 'DELETE FROM announcements WHERE id = ?';

    const id = req.params.id

    // jalankan query untuk melakukan pencarian data
    koneksi.query(querySearch, req.params.id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            // jalankan query update
            const coverId = rows[0].coverId
            koneksi.query(queryDeleteCover, coverId, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                }
        
                // jika id yang dimasukkan sesuai dengan data yang ada di db
                koneksi.query(queryDeleteNews, id, (err, rows, field) => {
                    // error handling
                    if (err) {
                        return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                    }
            
                    // jika id yang dimasukkan sesuai dengan data yang ada di db
                    newLogs(req, "Berhasil menghapus data pengumuman")
                    return res.status(200).json({ success: true, message: 'Data berhasil dihapus' });
                });
            });
        } else {
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
        }
    });
});

// read data / get data
app.get('/api/journals/count', (req, res) => {
    // buat query sql
    const querySql = 'SELECT COUNT(id) as total FROM journals';

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.post('/api/journals',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlMedias = 'INSERT INTO m_medias SET ?';
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'INSERT INTO journals SET ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (req.files.length === 0) {
        return res.status(500).json({ success: false, message: 'Harap pilih cover terlebih dahulu' });
    }

    // Display uploaded image for user validation
    const reqFile = req.files[0]
    koneksi.query(querySqlMedias, {
        file_name: reqFile.path,
        location: reqFile.path
    }, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }

        if (rows.affectedRows > 0) {

            const coverId = rows.insertId
            
            koneksi.query(querySqlAuthor, header, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
                console.log(rows[0].id)
                if (rows.length > 0) {
                    const authorId = rows[0].id

                    koneksi.query(querySql, {
                        title: body.title,
                        abstract: body.abstract,
                        keywords: body.keywords,
                        authors: body.authors,
                        coverId: coverId,
                        authorId: authorId
                    }, (err, rows, field) => {
                        // error handling
                        if (err) {
                            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                        }
                
                        // jika request berhasil
                        newLogs(req, "Berhasil menambahkan data jurnal")
                        res.status(201).json({ success: true, message: 'Berhasil insert data!' });
                    });
                } else {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
            });

        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!' });
        }

    });
});

// read data / get data
app.put('/api/journals/:id',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlAuthor = 'SELECT id FROM users WHERE active_key = ?';
    const querySql = 'UPDATE journals SET ? WHERE id = ?';

    const body = req.body
    const header = req.headers["x-api-key"]
    const id = req.params.id
    console.log(header)

    // let uploadNew = multer({ storage: storage, fileFilter: helpers.imageFilter }).single('cover');

    koneksi.query(querySqlAuthor, header, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
        console.log(rows[0].id)
        if (rows.length > 0) {
            const authorId = rows[0].id

            koneksi.query(querySql, [
                {
                    title: body.title,
                    abstract: body.abstract,
                    keywords: body.keywords,
                    authors: body.authors,
                },
                id
            ], (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
        
                // jika request berhasil
                newLogs(req, "Berhasil mengubah data jurnal")
                res.status(201).json({ success: true, message: 'Berhasil update data!' });
            });
        } else {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }
    });
});

app.get('/api/journals', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)
    const q = req.query.q

    console.log(page, " | ", limit, " | ", offset)

    const querySql = "SELECT journals.*, m_medias.file_name, m_medias.location, users.username FROM journals JOIN m_medias ON m_medias.id = journals.coverId JOIN users ON users.id = journals.authorId WHERE journals.title LIKE ? OR journals.abstract LIKE ? ORDER BY journals.id DESC LIMIT ? OFFSET ?";

    // jalankan query
    koneksi.query(querySql, [
        '%'+q+'%',
        '%'+q+'%',
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

app.get('/api/journals/:id', (req, res) => {
    // buat query sql
    const id = req.params.id

    const querySql = "SELECT journals.*, m_medias.file_name, m_medias.location, users.username FROM journals JOIN m_medias ON m_medias.id = journals.coverId JOIN users ON users.id = journals.authorId WHERE journals.id = ?";

    // jalankan query
    koneksi.query(querySql, id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

// update data
app.delete('/api/journals/:id', (req, res) => {
    // buat variabel penampung data dan query sql
    const querySearch = 'SELECT coverId FROM journals WHERE id = ?';
    const queryDeleteCover = 'DELETE FROM m_medias WHERE id = ?';
    const queryDeleteNews = 'DELETE FROM journals WHERE id = ?';

    const id = req.params.id

    // jalankan query untuk melakukan pencarian data
    koneksi.query(querySearch, req.params.id, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika id yang dimasukkan sesuai dengan data yang ada di db
        if (rows.length) {
            // jalankan query update
            const coverId = rows[0].coverId
            koneksi.query(queryDeleteCover, coverId, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                }
        
                // jika id yang dimasukkan sesuai dengan data yang ada di db
                koneksi.query(queryDeleteNews, id, (err, rows, field) => {
                    // error handling
                    if (err) {
                        return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
                    }
            
                    // jika id yang dimasukkan sesuai dengan data yang ada di db
                    newLogs(req, "Berhasil menghapus data jurnal")
                    return res.status(200).json({ success: true, message: 'Data berhasil dihapus' });
                });
            });
        } else {
            return res.status(404).json({ success: false, message: 'Data tidak ditemukan!' });
        }
    });
});

app.get('/api/logs', (req, res) => {
    // buat query sql
    const page = Number(req.query.page)
    const limit = Number(req.query.limit)
    const offset = (page * limit)

    console.log(page, " | ", limit, " | ", offset)

    const querySql = "SELECT logs.* FROM logs ORDER BY logs.id DESC LIMIT ? OFFSET ?";

    // jalankan query
    koneksi.query(querySql, [
        limit, offset
    ], (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

app.get('/api/home', (req, res) => {
    const querySql = "SELECT home.*, m_medias.file_name, m_medias.location FROM home JOIN m_medias ON m_medias.id = home.bannerId";

    // jalankan query
    koneksi.query(querySql, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.length)
            res.status(200).json({ success: true, data: rows });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

app.put('/api/home', 
upload.any(), (req, res) => {
    // buat query sql
    const querySql = "UPDATE home SET ?";

    // jalankan query
    koneksi.query(querySql, req.body, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Ada kesalahan', error: err });
        }

        // jika request berhasil
        if (rows.affectedRows > 0)
            res.status(200).json({ success: true, message: 'Berhasil mengubah data' });
        else 
            return res.status(200).json({ success: false, data: [], message: 'Data tidak ditemukan!' });
    });
});

// read data / get data
app.put('/api/home/banner',
    upload.any(),
    (req, res) => {
    // buat query sql
    const querySqlMedias = 'INSERT INTO m_medias SET ?';
    const querySql = "UPDATE home SET ?"

    if (req.fileValidationError) {
        return res.send(req.fileValidationError);
    }
    else if (req.files.length === 0) {
        return res.status(500).json({ success: false, message: 'Harap pilih cover terlebih dahulu' });
    }

    // Display uploaded image for user validation
    const reqFile = req.files[0]
    koneksi.query(querySqlMedias, {
        file_name: reqFile.path,
        location: reqFile.path
    }, (err, rows, field) => {
        // error handling
        if (err) {
            return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
        }

        if (rows.affectedRows > 0) {

            const coverId = rows.insertId
            
            koneksi.query(querySql, {
                bannerId: coverId,
            }, (err, rows, field) => {
                // error handling
                if (err) {
                    return res.status(500).json({ success: false, message: 'Gagal insert data!', error: err });
                }
        
                // jika request berhasil
                newLogs(req, "Berhasil mengubah home")
                res.status(201).json({ success: true, message: 'Berhasil mengubah home' });
            });

        } else {
            return res.status(500).json({ success: false, message: 'Berhasil mengubah home' });
        }

    });
});

// buat server nya
app.listen(PORT, () => console.log(`Server running at port: ${PORT}`));